pub static TYPICAL_HUMAN_STRENGTH: usize = 10;

use crate::size_modifier::length_factor;

pub fn typical_strength(size: i8) -> usize {
    (TYPICAL_HUMAN_STRENGTH as f32 * length_factor(size)).floor() as usize
}

pub fn basic_lift(strength: usize) -> f32 {
    (strength * strength) as f32 / 5_f32
}
