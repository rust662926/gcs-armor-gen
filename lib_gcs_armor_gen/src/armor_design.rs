use color_eyre::eyre::{eyre, OptionExt, Result};
use uuid::Uuid;

use crate::config::accessory::{CostModifier, WeightModifier};
use crate::config::exoskeleton::Exoskeleton;
use crate::config::{self};
use crate::gcs::{GcsEquipmentFeature, GcsEquipmentItem, GcsEquipmentModifier};
use config::accessory::Accessory;
use config::armor_shape::ArmorShape;
use config::construction::Construction;
use config::design::Design;
use config::material::Material;
use config::Config;

use crate::concealment::Concealment;
use crate::tech_level::TechLevel;

#[derive(Debug)]
/// Armor Specification, i.e. all the information needed to generate armor
pub struct ArmorDesign<'a> {
    tech_level: TechLevel,

    /// Shape, i.e. which hit locations does this piece of armor cover?
    shape: &'a ArmorShape,

    /// The name given to the armor
    name: String,

    /// Size Modifier, as the GURPS definition of a Size Modifier, an exponential scale with human size as 0
    size_modifier: i8,

    /// Material, i.e. what material is this made of?
    /// Materials are also TL-dependent, affecting the minimum TL for this item
    /// Materials may be combined, apparently, but that sounds complicated
    /// Materials may be rigid or flexible, this affects what material construction can be used
    /// Materials impose a maximum and minimum DR
    material: Material,

    /// Construction, i.e. how are those materials used
    /// Construction also imposes a minimum DR
    construction: &'a Construction,

    /// DR, i.e. how much protection should this material provide
    /// this is used to derive cost, weight, etc.
    /// note that materials and constructions can impose upper and lower limits on DR
    dr: u32,

    /// Accessories, i.e. anything else added to the armour
    /// These might be complicated
    accessories: Vec<&'a Accessory>,

    /// Legality class, as GURPS Basic Set 267 (ranges from 0 to 4)
    legality_class: u8,

    /// Whether the armour is rigid
    rigid: bool,

    /// Whether this armor is designed to be concealed, and as what
    concealment: Concealment,

    /// What exoskeleton this armour uses, if any
    exoskeleton: Exoskeleton,
}

impl<'a> ArmorDesign<'a> {
    fn get_dr_modifiers(&self) -> Vec<(String, f32)> {
        let mut modifiers: Vec<(String, f32)> = vec![];
        let mut material_multipliers: Vec<(String, f32)> = self
            .material
            .dr_multipliers()
            .iter()
            .map(|(label, value)| (label.clone(), *value * self.dr as f32))
            .collect();
        let mut construction_multipliers: Vec<(String, f32)> = self
            .construction
            .dr_multipliers()
            .iter()
            .map(|(label, value)| (label.clone(), value * self.dr as f32))
            .collect();
        let mut construction_additives: Vec<(String, f32)> = self
            .construction
            .dr_additions()
            .iter()
            .map(|(label, value)| (label.clone(), *value as f32))
            .collect();
        modifiers.append(&mut material_multipliers);
        modifiers.append(&mut construction_multipliers);
        modifiers.append(&mut construction_additives);
        modifiers
    }

    pub fn tech_level(&self) -> TechLevel {
        self.tech_level
    }

    pub fn size_modifier(&self) -> i8 {
        self.size_modifier
    }

    pub fn rigid(&self) -> bool {
        self.rigid
    }
}

fn calculate_real_weight(weight: f32, modifiers: &[GcsEquipmentModifier]) -> f32 {
    let mut additions = 0.0f32;
    let mut multipliers = 1.0f32;
    for modifier in modifiers {
        match modifier {
            GcsEquipmentModifier::EqpModifier { weight, .. } => {
                if let Some(first_char) = weight.chars().next() {
                    match first_char {
                        'x' => {
                            let weight_mul: f32 = weight.as_str()[1..].parse().unwrap();
                            multipliers *= weight_mul;
                        }
                        _ => {
                            let weight_add: f32 = weight.parse().unwrap();
                            additions += weight_add;
                        }
                    }
                }
            }
        }
    }
    weight * multipliers + additions
}

impl<'a> From<ArmorDesign<'a>> for GcsEquipmentItem {
    fn from(value: ArmorDesign) -> Self {
        let armor_weight: f32 = value.shape.square_footage(value.size_modifier)
            * value.material.weight_mul()
            * value.construction.weight_mul()
            * value.dr as f32;
        let armor_value: f32 = armor_weight * value.material.cost() * value.construction.cost_mul();
        // todo: round cost and weight to 2sf?
        let all_locations = value.shape.covered_locations();
        let features: Vec<GcsEquipmentFeature> = value
            .get_dr_modifiers()
            .iter()
            .flat_map(|(label, value)| {
                all_locations
                    .iter()
                    .map(|location| GcsEquipmentFeature::DrBonus {
                        location: location.clone(),
                        specialization: Some(label.clone()),
                        amount: value.floor() as i32,
                    })
            })
            .collect();
        let mut modifiers: Vec<GcsEquipmentModifier> = value
            .accessories
            .iter()
            .map(|acc| {
                let cost = match acc.cost() {
                    CostModifier::None => String::new(),
                    CostModifier::Additive(value) => value.to_string(),
                    CostModifier::BaseMultiple(value) => format!("x{}", value),
                    CostModifier::SurfaceAreaMultiple(mul) => {
                        (value.shape.square_footage(value.size_modifier) * mul).to_string()
                    }
                };
                let weight = match acc.weight() {
                    WeightModifier::None => String::new(),
                    WeightModifier::Additive(value) => value.to_string(),
                    WeightModifier::BaseMultiple(value) => format!("x{}", value),
                    WeightModifier::SurfaceAreaMultiple(mul) => {
                        (value.shape.square_footage(value.size_modifier) * mul).to_string()
                    }
                };
                let features = acc
                    .dr_multipliers()
                    .iter()
                    .flat_map(|(label, mul)| {
                        all_locations
                            .iter()
                            .map(|location| GcsEquipmentFeature::DrBonus {
                                location: location.clone(),
                                specialization: Some(label.clone()),
                                amount: (*mul * value.dr as f32).floor() as i32,
                            })
                    })
                    .collect();
                GcsEquipmentModifier::EqpModifier {
                    id: Uuid::new_v4().into(),
                    name: acc.name().into(),
                    reference: acc.reference().into(),
                    notes: acc.description().into(),
                    tags: vec![],
                    tech_level: acc.tech_level().into(),
                    cost,
                    weight,
                    features,
                }
            })
            .collect();
        let mut notes: Vec<String> = vec![];
        if !value.rigid {
            notes.push("Flexible".into());
        }
        if !value.concealment.is_none() {
            notes.push(value.concealment.as_note().into());
        }

        // Calculate modifiers for the exoskeleton
        if !value.exoskeleton.is_none() {
            let mut exoskeleton_notes: Vec<String> = vec![];
            // striking ST, lifting ST, move bonus
            let mut exoskeleton_features: Vec<GcsEquipmentFeature> = vec![];
            exoskeleton_features.push(GcsEquipmentFeature::AttributeBonus {
                limitation: Some("lifting_only".into()),
                attribute: "st".into(),
                amount: value.exoskeleton.lifting_strength_bonus() as i32,
                per_level: None,
            });
            exoskeleton_features.push(GcsEquipmentFeature::AttributeBonus {
                limitation: Some("striking_only".into()),
                attribute: "st".into(),
                amount: value.exoskeleton.striking_strength_bonus() as i32,
                per_level: None,
            });

            let move_bonus = value
                .exoskeleton
                .frame_move_bonus(value.size_modifier, value.tech_level);
            if move_bonus > 0 {
                exoskeleton_features.push(GcsEquipmentFeature::AttributeBonus {
                    limitation: None,
                    attribute: "basic_move".into(),
                    amount: move_bonus as i32,
                    per_level: None,
                });
            }
            let mut frame_dr: i32 = value
                .exoskeleton
                .frame_dr(value.size_modifier, value.tech_level)
                as i32;
            for (mod_name, dr) in value.get_dr_modifiers() {
                if mod_name == "all" {
                    frame_dr -= dr as i32;
                }
            }
            if frame_dr > 0 {
                for location in all_locations {
                    exoskeleton_features.push(GcsEquipmentFeature::DrBonus {
                        location: location.clone(),
                        specialization: Some(
                            "collisions, falls, impacts and swung melee attacks".into(),
                        ),
                        amount: frame_dr,
                    });
                }
            }

            // Features that can only be part of the description
            let exoskeleton_name: String;
            match value.exoskeleton {
                Exoskeleton::None => exoskeleton_name = "This should not happen".into(),
                Exoskeleton::CombatWalker { .. } => {
                    exoskeleton_name = "Combat Walker".into();
                    if value.tech_level.level() <= 9 {
                        exoskeleton_notes.push("Cannot crawl or get up from prone".into());
                        exoskeleton_features.push(GcsEquipmentFeature::ConditionalModifier {
                            situation: "to tasks that require a firm grip".into(),
                            amount: -4,
                            per_level: None,
                        });
                    } else if value.tech_level.level() <= 10 {
                        exoskeleton_features.push(GcsEquipmentFeature::ConditionalModifier {
                            situation: "to tasks that require a firm grip".into(),
                            amount: -2,
                            per_level: None,
                        });
                    }
                }
                Exoskeleton::ExoskeletonFrame { .. } => {
                    exoskeleton_name = "Powered Exoskeleton".into()
                }
                Exoskeleton::CyberSuit { .. } => {
                    exoskeleton_name = "Cybersuit".into();
                    exoskeleton_notes.push(
                        "Can be used without battlesuit training to only support its own weight"
                            .into(),
                    )
                }
            }

            let non_exoskeleton_weight = calculate_real_weight(armor_weight, &modifiers);
            let super_jump = value.exoskeleton.frame_super_jump(
                value.size_modifier,
                value.tech_level,
                non_exoskeleton_weight,
            );
            if super_jump > 0 {
                exoskeleton_notes.push(format!("Super Jump {super_jump}"));
            } else if !value.exoskeleton.frame_can_jump(
                value.size_modifier,
                value.tech_level,
                non_exoskeleton_weight,
            ) {
                exoskeleton_notes.push("Cannot jump".into());
            }
            notes.push(format!(
                "SM{}",
                value
                    .exoskeleton
                    .frame_size_modifier(value.size_modifier, value.tech_level)
            ));
            exoskeleton_notes.push(format!("Occupant SM{}", value.size_modifier));

            let power_consumption = value
                .exoskeleton
                .frame_power_consumption(value.size_modifier, value.tech_level);
            exoskeleton_notes.push(format!(
                "Exoskeleton lasts for {power_consumption} hours per D cell"
            ));

            // Cost and weight
            let exoskeleton_value = value
                .exoskeleton
                .frame_cost(value.size_modifier, value.tech_level);
            let exoskeleton_weight = value
                .exoskeleton
                .frame_weight(value.size_modifier, value.tech_level);

            let exoskeleton_modifier = GcsEquipmentModifier::EqpModifier {
                id: Uuid::new_v4().into(),
                name: exoskeleton_name,
                reference: "".into(),
                notes: exoskeleton_notes.join("; "),
                tags: vec![],
                tech_level: value.tech_level().into(),
                cost: exoskeleton_value.to_string(),
                weight: exoskeleton_weight.to_string(),
                features: exoskeleton_features,
            };
            modifiers.push(exoskeleton_modifier);

            // exoskeletons also don't count towards encumberance when worn
            let total_weight = calculate_real_weight(armor_weight, &modifiers);
            modifiers.push(GcsEquipmentModifier::EqpModifier {
                id: Uuid::new_v4().into(),
                name: "Exoskeletons support their own weight and don't count towards encumberance"
                    .into(),
                reference: "B285".into(),
                notes: "Toggle this if the equipment is carried but not equipped".into(),
                tags: vec![],
                tech_level: "".into(),
                cost: "".into(),
                weight: format!("-{} lb", total_weight),
                features: vec![],
            })
        } else {
            notes.push(format!("SM{}", value.size_modifier))
        }

        Self::Equipment {
            id: Uuid::new_v4().into(),
            description: value.name,
            reference: value.material.reference().into(),
            notes: notes.join(";"),
            tech_level: value.tech_level.into(),
            legality_class: value.legality_class.to_string(),
            tags: vec!["Body Armor".into()],
            modifiers,
            quantity: 1,
            value: armor_value.floor() as i32,
            weight: format!("{} lb", armor_weight),
            features, // all non-accessory DR as features
            equipped: None,
            calc: None,
        }
    }
}

pub fn load_armor_designs(config: &Config) -> Result<Vec<ArmorDesign<'_>>> {
    let designs = config
        .designs()
        .iter()
        .map(|d| load_armor_design(config, d))
        .collect::<Result<Vec<_>, _>>()?;
    Ok(designs)
}

pub fn load_armor_design<'a>(
    config: &'a config::Config,
    serial_design: &'a Design,
) -> Result<ArmorDesign<'a>> {
    let tech_level = serial_design.tech_level();
    let size_modifier = serial_design.size_modifier();
    let dr = serial_design.dr();

    let name = serial_design.name().to_string();

    let exoskeleton = serial_design.exoskeleton.clone();
    let exoskeleton_size_modifier = exoskeleton.frame_size_modifier(size_modifier, tech_level);

    let material = config
        .find_material(
            serial_design.material(),
            &serial_design.tech_level(),
            dr,
            exoskeleton_size_modifier,
        )
        .ok_or_eyre(format!(
            "No Material found for TL {}, DR {}, SM {}",
            &serial_design.tech_level(),
            dr,
            exoskeleton_size_modifier
        ))?;
    let construction = config
        .find_construction(serial_design.construction(), dr)
        .ok_or_eyre("No Construction found")?;
    let shape = config.find_shape(serial_design.shape())?;
    let rigid = construction.is_rigid(dr, material.unscaled_max_dr());
    let accessories = serial_design
        .accessories()
        .iter()
        .map(|name| {
            config
                .find_accessory(
                    name.as_ref(),
                    &serial_design.tech_level(),
                    serial_design.material(),
                    rigid,
                    shape.covered_locations(),
                    dr,
                    shape.percentage_covered(),
                    serial_design.accessories(),
                )
                .map_err(|e| e.join(";"))
        })
        .collect::<Result<Vec<_>, _>>()
        .map_err(|e| eyre!("{}", e))?;
    let legality_class = serial_design.legality_class();
    let concealment = serial_design.concealment().clone();
    concealment
        .is_eligible(dr, material.max_dr())
        .map_err(|e| eyre!(e))?;
    Ok(ArmorDesign {
        tech_level,
        shape,
        name,
        size_modifier,
        material,
        construction,
        dr,
        accessories,
        legality_class,
        rigid,
        concealment,
        exoskeleton,
    })
}
