use serde::{Deserialize, Serialize};

/// How this armor intends to be concealed
#[derive(Serialize, Deserialize, Debug, Default, Clone, PartialEq)]
pub enum Concealment {
    /// Not concealed as clothing
    #[default]
    None,
    /// Concealed under clothes (e.g. bulletproof vest), allowed with up to half max DR
    UnderClothes,
    /// Concealed as outerwear, allowed with up to half max DR
    Outerwear,
    /// Concealed as light clothing (T-shirt, evening wear, skin-tight suits, etc.), allowed with up to quarter max DR
    Light,
    /// Concealed as diaphanous clothing (swimwear, lingerie, etc.), allowed with up to one sixth max DR
    Diaphanous,
}

impl Concealment {
    pub(crate) fn is_none(&self) -> bool {
        matches!(self, Self::None)
    }

    pub(crate) fn as_note(&self) -> &'static str {
        match self {
            Self::None => "",
            Self::UnderClothes => "Concealable under clothes",
            Self::Outerwear => "Concealable as outerwear",
            Self::Light => "Concealable as light clothing",
            Self::Diaphanous => "Concealable as diaphanous clothing",
        }
    }

    fn max_frac(&self) -> f32 {
        match self {
            Self::None => 1.0,
            Self::UnderClothes => 0.5,
            Self::Outerwear => 0.5,
            Self::Light => 0.25,
            Self::Diaphanous => 1.0 / 6.0,
        }
    }

    pub fn is_eligible(&self, dr: u32, max_dr: u32) -> Result<(), String> {
        let frac = dr as f32 / max_dr as f32;
        let max_frac = self.max_frac();
        match frac > max_frac {
            true => Err(format!(
                "Concealment '{self:?}' allows DR ratio of up to {max_frac}, found {frac}"
            )),
            false => Ok(()),
        }
    }

    pub fn all_variants() -> &'static [Concealment] {
        &[
            Self::None,
            Self::UnderClothes,
            Self::Outerwear,
            Self::Light,
            Self::Diaphanous,
        ]
    }
}
