use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize, Debug)]
pub struct GcsEquipmentCalc {
    extended_value: i32,
    extended_weight: String, //< weight as "N lb"
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum GcsEquipmentFeature {
    ConditionalModifier {
        situation: String,
        amount: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        per_level: Option<bool>,
    },
    DrBonus {
        location: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        specialization: Option<String>,
        amount: i32,
    },
    AttributeBonus {
        // blank, "lifting_only", "striking_only", "throwing_only" (limitations only apply to strength bonuses, hard-coded into gcs)
        #[serde(skip_serializing_if = "Option::is_none")]
        limitation: Option<String>,
        attribute: String, // "st", "basic_move"
        amount: i32,
        #[serde(skip_serializing_if = "Option::is_none")]
        per_level: Option<bool>,
    },
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum GcsEquipmentModifier {
    EqpModifier {
        id: String, // UuidV4
        name: String,
        reference: String,
        notes: String,
        #[serde(skip_serializing_if = "Vec::is_empty")]
        tags: Vec<String>,
        tech_level: String,
        cost: String,   // +100 and the like
        weight: String, //< as "N lb"
        #[serde(skip_serializing_if = "Vec::is_empty")]
        features: Vec<GcsEquipmentFeature>,
    },
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum GcsEquipmentItem {
    Equipment {
        id: String, // create new ones with Uuid::new_v4()
        description: String,
        reference: String,
        notes: String,
        tech_level: String,     //< M+N^
        legality_class: String, //< could probably be an int
        #[serde(skip_serializing_if = "Vec::is_empty")]
        tags: Vec<String>,
        #[serde(skip_serializing_if = "Vec::is_empty")]
        modifiers: Vec<GcsEquipmentModifier>,
        quantity: u32,
        value: i32,
        weight: String, //< weight as "N lb"
        #[serde(skip_serializing_if = "Vec::is_empty")]
        features: Vec<GcsEquipmentFeature>,
        #[serde(skip_serializing_if = "Option::is_none")]
        equipped: Option<bool>, //< might exist in the model, though not usually
        #[serde(skip_serializing_if = "Option::is_none")]
        calc: Option<GcsEquipmentCalc>,
    },
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum GcsEquipmentLib {
    EquipmentList {
        version: u32,
        rows: Vec<GcsEquipmentItem>,
    },
}

impl From<GcsEquipmentItem> for GcsEquipmentLib {
    fn from(item: GcsEquipmentItem) -> Self {
        Self::EquipmentList {
            version: 4,
            rows: vec![item],
        }
    }
}

impl From<Vec<GcsEquipmentItem>> for GcsEquipmentLib {
    fn from(items: Vec<GcsEquipmentItem>) -> Self {
        Self::EquipmentList {
            version: 4,
            rows: items,
        }
    }
}
