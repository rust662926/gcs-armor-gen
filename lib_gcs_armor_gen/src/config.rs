use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

use crate::{size_modifier, tech_level::TechLevel};
use color_eyre::eyre::{bail, eyre, OptionExt, Result};

pub mod accessory;
pub mod armor_shape;
pub mod construction;
pub mod design;
pub mod exoskeleton;
pub mod material;
pub mod options;

use accessory::Accessory;
use armor_shape::ArmorShape;
use construction::Construction;
use design::Design;
use material::Material;
use options::Options;

/// Config, describing as much of the data model in a way that doesn't make user modification difficult
#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Config {
    #[serde(default = "Default::default")]
    armor_shapes: Vec<armor_shape::ArmorShape>,
    #[serde(default = "Default::default")]
    materials: Vec<material::Material>,
    #[serde(default = "Default::default")]
    constructions: Vec<Construction>,
    #[serde(default = "Default::default")]
    accessories: Vec<Accessory>,
    #[serde(default = "Default::default")]
    designs: Vec<Design>,
    #[serde(
        skip_serializing_if = "Options::is_default",
        default = "Default::default"
    )]
    options: Options,
}

impl From<Vec<Design>> for Config {
    fn from(value: Vec<Design>) -> Self {
        Self {
            designs: value,
            ..Default::default()
        }
    }
}

impl Config {
    pub fn append(&mut self, mut other: Self) {
        self.armor_shapes.append(&mut other.armor_shapes);
        self.materials.append(&mut other.materials);
        self.constructions.append(&mut other.constructions);
        self.accessories.append(&mut other.accessories);
        self.designs.append(&mut other.designs);
        self.options.merge_in(other.options());
    }

    pub fn find_material(
        &self,
        name: &str,
        tech_level: &TechLevel,
        dr: u32,
        size_modifier: i8,
    ) -> Option<Material> {
        let materials = match self.options().max_dr_scales_with_size() {
            true => Material::scale_max_dr(
                self.materials.clone(),
                size_modifier::length_factor(size_modifier),
            ),
            false => self.materials.clone(),
        };

        let material_results: Vec<Material> = materials
            .into_iter()
            .filter(|m| m.name() == name && m.is_eligible(tech_level, dr).is_ok())
            .collect();
        material_results.last().cloned()
    }

    pub fn find_shape(&self, name: &str) -> Result<&ArmorShape> {
        let shape_results: Vec<&ArmorShape> = self
            .armor_shapes()
            .iter()
            .filter(|s| s.name() == name)
            .collect();
        if shape_results.len() != 1 {
            bail!("{shape_results:?} is not a unique entry!");
        }
        shape_results.first().copied().ok_or_eyre("No shape found")
    }

    pub(crate) fn find_construction(&self, name: &str, dr: u32) -> Option<&Construction> {
        let mut construction_results: Vec<&Construction> = self
            .constructions()
            .iter()
            .filter(|c| c.name() == name && dr >= c.min_dr())
            .collect();
        // instead of enforcing uniqueness, take the one with the highest min DR
        construction_results.sort_by_key(|c| c.min_dr());
        construction_results.last().copied()
    }

    #[allow(clippy::too_many_arguments)]
    pub(crate) fn find_accessory(
        &self,
        name: &str,
        tech_level: &TechLevel,
        material: &str,
        is_rigid: bool,
        hit_locations: &[String],
        dr: u32,
        coverage: f32,
        accessories: &[String],
    ) -> Result<&Accessory, Vec<String>> {
        // the result os a vector of vector of strings
        // a vector of strings for the first
        let possible_results: Vec<_> = self
            .accessories()
            .iter()
            .filter(|a| a.name() == name)
            .map(|a| {
                a.is_eligible(
                    tech_level,
                    material,
                    is_rigid,
                    hit_locations,
                    dr,
                    coverage,
                    accessories,
                )
                .map_err(|e| e.join(","))
            })
            .collect();
        match possible_results.iter().any(Result::is_ok) {
            true => Ok(possible_results
                .into_iter()
                .filter_map(Result::ok)
                .collect::<Vec<_>>()
                .last()
                .unwrap()),
            false => Err(possible_results
                .into_iter()
                .filter_map(Result::err)
                .collect::<Vec<_>>()),
        }
    }

    pub fn validate(&self) -> Result<()> {
        // For every material name/TL pairing, there must be a contiguous DR range
        // ???: How do I check that?

        // Armor shapes must have unique names
        let mut shape_name = HashMap::<&str, usize>::new();
        self.armor_shapes.iter().for_each(|s| {
            shape_name
                .entry(s.name())
                .and_modify(|v| *v += 1)
                .or_insert(1);
        });
        let duplicate_shapes = shape_name
            .iter()
            .filter_map(|(name, count)| match count {
                1 => None,
                _ => Some(name),
            })
            .collect::<Vec<_>>();
        if !duplicate_shapes.is_empty() {
            bail!(
                "Multiple shapes with the same name found for the following: {duplicate_shapes:?}"
            )
        }

        // Armor constructions must have unique name/min_dr combinations
        let mut construction_name_dr = HashMap::<(&str, u32), usize>::new();
        self.constructions.iter().for_each(|s| {
            construction_name_dr
                .entry((s.name(), s.min_dr()))
                .and_modify(|v| *v += 1)
                .or_insert(1);
        });
        let duplicate_constructions = construction_name_dr
            .iter()
            .filter_map(|(k, count)| match count {
                1 => None,
                _ => Some(format!("{} Min DR {}", k.0, k.1)),
            })
            .collect::<Vec<_>>();
        if !duplicate_constructions.is_empty() {
            bail!("Multiple constructions with the same name and min DR found for the following: {duplicate_constructions:?}")
        }

        // A Material's allowed constructions must refer to constructions that actually exist
        let required_constructions = self
            .materials
            .iter()
            .flat_map(|m| m.construction().iter())
            .map(|s| s.as_ref())
            .collect::<HashSet<&str>>();
        let found_constructions = self
            .constructions
            .iter()
            .map(Construction::name)
            .collect::<HashSet<&str>>();
        let missing_constructions = required_constructions
            .difference(&found_constructions)
            .collect::<HashSet<_>>();
        if !missing_constructions.is_empty() {
            bail!("Missing constructions with names: {missing_constructions:?}");
        }

        // A Construction's restricted shapes must refer to shapes that actually exist.
        let required_shapes = self
            .constructions
            .iter()
            .flat_map(|c| c.restricted_to_locations().iter())
            .map(|s| s.as_ref())
            .collect::<HashSet<&str>>();
        let found_shapes = self
            .armor_shapes
            .iter()
            .flat_map(|s| s.covered_locations().iter().map(|l| l.as_ref()))
            .collect::<HashSet<&str>>();
        let missing_shapes = required_shapes
            .difference(&found_shapes)
            .collect::<HashSet<_>>();
        if !missing_shapes.is_empty() {
            bail!("Missing shapes with names: {missing_shapes:?}");
        }

        for design in self.designs() {
            let tl = design.tech_level;
            let dr = design.dr;
            let sm = design
                .exoskeleton
                .frame_size_modifier(design.size_modifier, tl);
            let construction = self
                .find_construction(design.construction(), dr)
                .ok_or_else(|| {
                    eyre!(
                        "Design named {} has DR {} and wants construction {}, which can't be found",
                        design.name,
                        dr,
                        design.construction()
                    )
                })?;
            let material = self
                .find_material(design.material(), &tl, dr, sm)
                .ok_or_else(|| eyre!("Design named {} has DR {}, TL {}, SM {} and wants material {}, which can't be found", design.name, dr, tl, sm, design.material()))?;
            design.exoskeleton.validate(
                design.size_modifier(),
                design.tech_level(),
                construction.is_rigid(dr, material.unscaled_max_dr()),
            )?;
        }
        Ok(())
    }

    pub fn armor_shapes(&self) -> &[ArmorShape] {
        self.armor_shapes.as_ref()
    }

    pub fn materials(&self) -> &[Material] {
        self.materials.as_ref()
    }

    pub fn constructions(&self) -> &[Construction] {
        self.constructions.as_ref()
    }

    pub fn accessories(&self) -> &[Accessory] {
        self.accessories.as_ref()
    }

    pub fn designs(&self) -> &[Design] {
        self.designs.as_ref()
    }
    pub fn designs_mut(&mut self) -> &mut Vec<Design> {
        &mut self.designs
    }

    pub fn options(&self) -> &Options {
        &self.options
    }
}
