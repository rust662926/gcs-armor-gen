use color_eyre::eyre::{bail, Error, Result};
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::str::FromStr;

/// Specification of a Tech Level
#[derive(Eq, Serialize, Deserialize, Debug, Clone, Copy)]
#[serde(try_from = "String", into = "String")]
pub struct TechLevel {
    /// Basic tech level, usually from 0 to 12
    level: u8,
    /// Tech level that the TL diverged from (e.g. a TL7 society that never discovered petroleum might be TL 4+3)
    diverged_from: Option<u8>,
    /// Whether technology incorporates supernatural or fantastical phenomena
    superscience: bool,
}

impl PartialEq for TechLevel {
    fn eq(&self, other: &Self) -> bool {
        self.level == other.level && self.superscience == other.superscience
    }
}

impl std::hash::Hash for TechLevel {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.level.hash(state);
        self.superscience.hash(state);
    }
}

impl Ord for TechLevel {
    fn cmp(&self, other: &Self) -> Ordering {
        let ord: Ordering;
        if self.superscience && !other.superscience {
            ord = Ordering::Greater;
        } else if !self.superscience && other.superscience {
            ord = Ordering::Less;
        } else {
            ord = self.level.cmp(&other.level);
        }
        ord
    }
}

impl PartialOrd for TechLevel {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl TechLevel {
    pub fn level(&self) -> u8 {
        self.level
    }
}

impl From<TechLevel> for String {
    fn from(val: TechLevel) -> Self {
        let mut text;
        if let Some(diverged) = val.diverged_from {
            text = format!("{}+{}", diverged, val.level - diverged);
        } else {
            text = format!("{}", val.level);
        }
        if val.superscience {
            text.push('^');
        }
        text
    }
}

impl std::fmt::Display for TechLevel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let text: String = From::<TechLevel>::from(*self);
        write!(f, "{text}")
    }
}

impl TryFrom<String> for TechLevel {
    type Error = Error;
    fn try_from(value: String) -> Result<Self, Self::Error> {
        FromStr::from_str(value.as_ref())
    }
}

impl FromStr for TechLevel {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut s = s.trim();
        let superscience: bool = s.ends_with('^');
        if superscience {
            s = &s[..s.len() - 1];
        }
        let split_tl = s
            .split('+')
            .map(u8::from_str)
            .collect::<Result<Vec<u8>, _>>()?;
        match split_tl.len() {
            2 => {
                let first = split_tl.first().unwrap();
                let second = split_tl.get(1).unwrap();
                let level = first + second;
                let diverged_from = Some(*first);
                Ok(TechLevel {
                    level,
                    diverged_from,
                    superscience,
                })
            }
            1 => {
                let level = *split_tl.first().unwrap();
                let diverged_from = None;
                Ok(TechLevel {
                    level,
                    diverged_from,
                    superscience,
                })
            }
            _ => {
                bail!("Can't get one or two tech levels from {split_tl:?}")
            }
        }
    }
}
