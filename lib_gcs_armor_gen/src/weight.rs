use crate::strength::TYPICAL_HUMAN_STRENGTH;

/// Weight of the average human, in lbs
/// typical weight of 150lbs from https://gurb3d6.blogspot.com/2016/09/power-armor-powered-up.html
static TYPICAL_HUMAN_WEIGHT: f32 = 150.0;

// Can I reverse-engineer a human weight scale by ST?
// Unliving objects get 2x as much HP than humans for their weight, Homog./diffuse get 4x as much
// There's a formula of HP = 4x(cube root of weight in lbs) for unliving objects
// i.e. weight is proportional to HP^3
// 10 = n * 150^3

/// Basic Set p.558 states that HP is proportional to the cube root of weight in lbs
/// thus, weight is proportional to HP^3
/// Since we're working with typical strength, we can also assume ST=HP
pub fn typical_weight(strength: usize) -> f32 {
    let strength = strength as f32;
    TYPICAL_HUMAN_WEIGHT * (strength.powi(3)) / ((TYPICAL_HUMAN_STRENGTH as f32).powi(3))
}
