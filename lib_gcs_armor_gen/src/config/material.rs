use serde::{Deserialize, Serialize};
use std::{collections::HashMap, vec};

use crate::tech_level::TechLevel;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Material {
    /// The minimum Tech Level that this technology is available at
    tech_level: TechLevel,

    /// The name of the material
    name: String,

    /// Weight Multiplier, how much a solid piece of armor covering 1 square inch with DR 1 would weigh
    weight_mul: f32,

    /// cost per lb of material, this often changes with TL,
    /// but this is most simply served by duplicate entries for other TLs
    cost: f32,

    /// DR per inch of armor thickness by damage type (specific circumstance e.g. "piercing and cutting" or "all")
    /// These modifiers are additive, DR may be added or subtracted under certain circumstances
    dr_per_inch: f32,

    /// Conditional multipliers of DR
    /// e.g. ("all", 1.0) as the default
    /// [("all", 1/3), ("piercing and cutting", 2/3)] for something that has full DR against piercing and cutting, and 1/3 for everything else
    #[serde(
        skip_serializing_if = "Material::is_default_dr_multipliers",
        default = "Material::default_dr_multipliers"
    )]
    dr_multipliers: HashMap<String, f32>,

    /// Maximum DR possible with this material
    max_dr: u32,

    /// Maximum DR that isn't altered by size modifier scaling
    /// I can't deserialize this with the exact same value as max_dr, so I'll
    /// set it where needed and be smarter with the getter
    #[serde(skip)]
    unscaled_max_dr: Option<u32>,

    /// Minimum DR possible with this material
    #[serde(skip_serializing_if = "Option::is_none", default = "Default::default")]
    min_dr: Option<u32>,

    /// Notes for this material, often denoting special features of this material
    #[serde(skip_serializing_if = "Vec::is_empty", default = "Default::default")]
    notes: Vec<String>,

    /// Allowed construction types for this material
    construction: Vec<String>,

    /// Page reference to look up what the notes mean
    reference: String,
}

impl Material {
    pub fn construction(&self) -> &[String] {
        self.construction.as_ref()
    }

    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub(crate) fn tech_level(&self) -> &TechLevel {
        &self.tech_level
    }

    pub fn max_dr(&self) -> u32 {
        self.max_dr
    }

    pub fn unscaled_max_dr(&self) -> u32 {
        match self.unscaled_max_dr {
            Some(val) => val,
            None => self.max_dr,
        }
    }

    pub(crate) fn reference(&self) -> &str {
        self.reference.as_ref()
    }

    pub(crate) fn weight_mul(&self) -> f32 {
        self.weight_mul
    }

    pub(crate) fn cost(&self) -> f32 {
        self.cost
    }

    pub(crate) fn dr_multipliers(&self) -> &HashMap<String, f32> {
        &self.dr_multipliers
    }

    fn default_dr_multipliers() -> HashMap<String, f32> {
        [("all".into(), 1.0)].into()
    }

    fn is_default_dr_multipliers(multipliers: &HashMap<String, f32>) -> bool {
        *multipliers == Self::default_dr_multipliers()
    }

    pub fn is_eligible(&self, tech_level: &TechLevel, dr: u32) -> Result<(), String> {
        let mut ineligible_reasons: Vec<String> = vec![];
        let max_dr = self.max_dr;
        let min_dr = self.min_dr.unwrap_or_default();
        let min_tl = self.tech_level;

        if dr > self.max_dr {
            ineligible_reasons.push(format!("DR {dr} is higher than maximum {max_dr}"));
        }
        if dr < min_dr {
            ineligible_reasons.push(format!("DR {dr} is lower than minimum {min_dr}"))
        }

        if *tech_level < min_tl {
            ineligible_reasons.push(format!(
                "Tech Level {tech_level} is lower than minimum {min_tl}"
            ))
        }

        match ineligible_reasons.is_empty() {
            true => Ok(()),
            false => Err(ineligible_reasons.join("; ")),
        }
    }

    /// Take a list of variants of the same material and scale their Max DR
    /// A variant of the same material is a material with the same name (and may vary by TL)
    pub fn scale_material_group_max_dr<'a>(
        materials: impl IntoIterator<Item = &'a Self>,
        scale_multiplier: f32,
    ) -> Vec<Self> {
        // What are the rules for scaling materials?
        // Take the material with the highest Max DR and scale it
        // If that makes the material have a higher min DR than max DR, remove that material and try again
        // WOE BETIDE YOU IF THESE RANGES AREN'T CONTIGUOUS
        let mut materials: Vec<Material> = materials
            .into_iter()
            .map(std::borrow::ToOwned::to_owned)
            .collect();
        materials.sort_by_key(Material::max_dr);
        while !materials.is_empty() {
            let len = materials.len();
            let highest = materials.get_mut(len - 1).unwrap();
            highest.unscaled_max_dr = Some(highest.max_dr);
            highest.max_dr = (highest.max_dr as f32 * scale_multiplier).floor() as u32;
            if let Some(min_dr) = highest.min_dr {
                if min_dr > highest.max_dr() {
                    // scaling down the max DR makes this material variant non-viable
                    materials.pop();
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        materials
    }

    /// Take a list of materials and scale their Max DR
    /// This modifies the materials in the vector passed in
    pub fn scale_max_dr(materials: Vec<Self>, scale_multiplier: f32) -> Vec<Self> {
        // WARNING! This only works as expected when the name/TL variants are adjacent
        // The data should be sorted for this
        materials
            .chunk_by(|m1, m2| m1.name() == m2.name() && m1.tech_level() == m2.tech_level())
            .flat_map(|materials| Self::scale_material_group_max_dr(materials, scale_multiplier))
            .collect()
    }
}
