use serde::{Deserialize, Serialize};

/// Extra Options that might apply to armor design,
/// e.g. which design switches to enable
#[derive(Serialize, Deserialize, Debug, Clone, Default, PartialEq)]
pub struct Options {
    /// Whether Max DR scales with size modifier
    /// This is a house rule that makes armour for smaller creatures less
    /// protective, and armour for larger creatures may be more protective (if
    /// they can bear the extra weight).
    /// It assumes the main reason for Max DR is that there is a maximum
    /// thickness beyond which it stops making sense as armour, and that
    /// uniformly scaled-up armour has roughly the same shape, thus allowing
    /// thicker armour.
    #[serde(skip_serializing_if = "Option::is_none", default = "Default::default")]
    max_dr_scales_with_size: Option<bool>,
}

impl Options {
    pub fn is_default(&self) -> bool {
        *self == Default::default()
    }

    pub fn max_dr_scales_with_size(&self) -> bool {
        self.max_dr_scales_with_size.unwrap_or(false)
    }

    pub fn merge_in(&mut self, other: &Self) {
        if other.max_dr_scales_with_size.is_some() {
            self.max_dr_scales_with_size = other.max_dr_scales_with_size;
        }
    }
}
