use std::cmp::max;

use crate::weight::typical_weight;
use crate::{
    size_modifier::size_modifier_from_length,
    strength::{basic_lift, typical_strength},
    tech_level::TechLevel,
};

use color_eyre::eyre::{bail, Result};
use serde::{Deserialize, Serialize};

// Based on the design rules at https://gurb3d6.blogspot.com/2016/09/power-armor-powered-up.html

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub enum Exoskeleton {
    /// There is no enhancement to the wearer's strength
    #[default]
    None,
    /// The wearer's limbs are encased in machinery that mimics their movements
    /// with mechanical strength
    ExoskeletonFrame {
        /// A bonus applied to Lifting ST and Striking ST
        strength_bonus: usize,
        /// whether the armour is made for civilians - i.e. striking ST bonus is 2/3 or lifting ST
        civilian: bool,
    },
    /// The wearer is fully encased in a squat, bulky suit often covered in
    /// heavy armour
    CombatWalker {
        /// A bonus applied to Lifting ST and Striking ST
        strength_bonus: usize,
        /// whether the armour is made for civilians - i.e. striking ST bonus is 2/3 or lifting ST
        civilian: bool,
    },
    /// The wearer is encased in flexible, form-fitting material that emulates their muscles
    CyberSuit {
        /// A bonus applied to Lifting ST and Striking ST
        strength_bonus: usize,
        /// whether the armour is made for civilians - i.e. striking ST bonus is 2/3 or lifting ST
        civilian: bool,
    },
}

impl Exoskeleton {
    pub fn type_name(&self) -> &str {
        match *self {
            Self::None => "None",
            Self::ExoskeletonFrame { .. } => "ExoskeletonFrame",
            Self::CombatWalker { .. } => "CombatWalker",
            Self::CyberSuit { .. } => "CyberSuit",
        }
    }

    pub fn is_none(&self) -> bool {
        matches!(*self, Self::None)
    }

    pub fn is_civilian(&self) -> bool {
        match *self {
            Self::None => false,
            Self::ExoskeletonFrame { civilian, .. } => civilian,
            Self::CombatWalker { civilian, .. } => civilian,
            Self::CyberSuit { civilian, .. } => civilian,
        }
    }

    pub fn lifting_strength_bonus(&self) -> usize {
        match *self {
            Self::None => 0usize,
            Self::ExoskeletonFrame { strength_bonus, .. } => strength_bonus,
            Self::CombatWalker { strength_bonus, .. } => strength_bonus,
            Self::CyberSuit { strength_bonus, .. } => strength_bonus,
        }
    }

    pub fn striking_strength_bonus(&self) -> usize {
        let lifting_st = self.lifting_strength_bonus() as f32;
        match self.is_civilian() {
            true => (lifting_st * 2.0 / 3.0) as usize,
            false => lifting_st as usize,
        }
    }

    /// The strength of the typical wearer, plus the strength bonus
    fn total_strength(&self, occupant_size: i8) -> usize {
        let strength_bonus = match *self {
            Self::None => return 0usize,
            Self::ExoskeletonFrame {
                strength_bonus: strength,
                ..
            } => strength,
            Self::CombatWalker {
                strength_bonus: strength,
                ..
            } => strength,
            Self::CyberSuit {
                strength_bonus: strength,
                ..
            } => strength,
        };
        strength_bonus + typical_strength(occupant_size)
    }

    /// The basic lift, in lbs, of the exoskeleton and the wearer
    pub fn total_basic_lift(&self, occupant_size: i8) -> f32 {
        basic_lift(self.total_strength(occupant_size))
    }

    /// The basic lift, in lbs, added by the exoskeleton
    pub fn frame_basic_lift(&self, occupant_size: i8) -> f32 {
        self.total_basic_lift(occupant_size) - basic_lift(typical_strength(occupant_size))
    }

    fn frame_weight_tl_factor(tl: TechLevel) -> f32 {
        match tl.level() {
            12 => 3.0,
            11 => 2.0,
            10 => 1.5,
            9 => 1.0,
            _ => 0.0,
        }
    }

    /// The weight of all the machinery that enhances strength
    pub fn frame_weight(&self, occupant_size: i8, tl: TechLevel) -> f32 {
        let frame_weight_factor: f32 = 0.75;
        self.frame_basic_lift(occupant_size) * frame_weight_factor
            / Self::frame_weight_tl_factor(tl)
    }

    /// The cost of all the machinery that enhances strength
    pub fn frame_cost(&self, occupant_size: i8, tl: TechLevel) -> f32 {
        let frame_cost_factor: f32 = 1047.0;
        self.frame_weight(occupant_size, tl) * frame_cost_factor
    }

    fn frame_power_consumption_tl_factor(tl: TechLevel) -> f32 {
        // Progression of 1 at TL9, and multiply by 4 for every TL higher
        4f32.powi(tl.level() as i32 - 9)
    }

    /// How much power it takes to operate this exoskeleton, in hours per D cell
    /// This does not include the cost and weight of those D cells. Add those as accessories.
    pub fn frame_power_consumption(&self, occupant_size: i8, tl: TechLevel) -> f32 {
        18f32 * 60f32 * Self::frame_power_consumption_tl_factor(tl)
            / self.frame_basic_lift(occupant_size)
    }

    fn frame_height_tl_factor(tl: TechLevel) -> f32 {
        match tl.level() {
            12 => 3.0,
            11 => 2.0,
            10 => 1.5,
            9 => 1.0,
            _ => 0.0,
        }
    }

    /// Height of the suit in ft
    pub fn frame_height(&self, occupant_size: i8, tl: TechLevel) -> f32 {
        let typical_frame_height: f32 = 6.5;
        let typical_frame_basic_lift: f32 = 60.0;
        typical_frame_height
            * (self.frame_basic_lift(occupant_size)
                / Self::frame_height_tl_factor(tl)
                / typical_frame_basic_lift)
                .sqrt()
    }

    /// The size modifier of the whole exoskeleton
    /// Sufficiently strong exoskeletons take up more space
    pub fn frame_size_modifier(&self, occupant_size: i8, tl: TechLevel) -> i8 {
        let height_ft = self.frame_height(occupant_size, tl);
        let height_yds = height_ft / 3.0;
        let size_modifier = size_modifier_from_length(height_yds);
        match *self {
            Self::None => occupant_size,
            Self::CombatWalker { .. } => max(size_modifier + 1, occupant_size),
            _ => max(size_modifier, occupant_size),
        }
    }

    /// The maximum size modifier for this type of exoskeleton
    pub fn maximum_size_modifier(&self, occupant_size: i8) -> i8 {
        match *self {
            Self::None => occupant_size,
            Self::ExoskeletonFrame { .. } => occupant_size + 2,
            Self::CombatWalker { .. } => occupant_size + 3,
            Self::CyberSuit { .. } => occupant_size,
        }
    }

    fn with_strength_bonus(&self, strength_bonus: usize) -> Self {
        match *self {
            Self::None => Self::None,
            Self::CombatWalker { civilian, .. } => Self::CombatWalker {
                strength_bonus,
                civilian,
            },
            Self::ExoskeletonFrame { civilian, .. } => Self::ExoskeletonFrame {
                strength_bonus,
                civilian,
            },
            Self::CyberSuit { civilian, .. } => Self::CyberSuit {
                strength_bonus,
                civilian,
            },
        }
    }

    /// Maximum strength bonus, i.e. how big does it get from added machinery
    /// before it gets too large for the occupant to operate with their body?
    pub fn maximum_strength_bonus(&self, occupant_size: i8, tl: TechLevel) -> usize {
        // TODO: Don't recalculate every time, probably expensive!
        let max_sm = self.maximum_size_modifier(occupant_size);
        let mut max_st: usize = 0;
        for strength_bonus in 1.. {
            let size = self
                .with_strength_bonus(strength_bonus)
                .frame_size_modifier(occupant_size, tl);
            if size > max_sm {
                break;
            } else {
                max_st = strength_bonus;
            }
        }
        max_st
    }

    fn frame_stride_factor(&self) -> u8 {
        match *self {
            Self::None => 0,
            Self::ExoskeletonFrame { .. } => 5,
            Self::CombatWalker { .. } => 5,
            Self::CyberSuit { .. } => 6,
        }
    }

    /// Increased movement speed provided by the exoskeleton
    pub fn frame_move_bonus(&self, occupant_size: i8, tl: TechLevel) -> u8 {
        let height = self.frame_height(occupant_size, tl);
        let height_divisor = 6.0;
        // ??? This is probably Basic Move for the average human, but smaller
        // creatures don't *automatically* have lower Basic Move
        let move_subtraction = 5;

        let mut move_modifier: isize = ((height / height_divisor).sqrt() as u8
            * self.frame_stride_factor()) as isize
            - move_subtraction;
        if move_modifier < 0 {
            move_modifier = 0;
        }
        move_modifier as u8
    }

    fn frame_jump_factor(&self) -> f32 {
        match *self {
            Self::None => 0.0,
            Self::ExoskeletonFrame { .. } => 1.15,
            Self::CombatWalker { .. } => 1.0,
            Self::CyberSuit { .. } => 1.15,
        }
    }

    pub fn frame_can_jump(&self, occupant_size: i8, tl: TechLevel, extra_weight: f32) -> bool {
        let total_weight = self.frame_weight(occupant_size, tl)
            + extra_weight
            + typical_weight(typical_strength(occupant_size));
        self.total_basic_lift(occupant_size) * self.frame_jump_factor() >= total_weight / 5.0
    }

    pub fn frame_super_jump(&self, occupant_size: i8, tl: TechLevel, extra_weight: f32) -> u8 {
        if !self.frame_can_jump(occupant_size, tl, extra_weight) {
            return 0;
        }
        let total_weight = self.frame_weight(occupant_size, tl)
            + extra_weight
            + typical_weight(typical_strength(occupant_size));
        let lift_mul =
            self.total_basic_lift(occupant_size) * self.frame_jump_factor() / (total_weight / 5.0);
        lift_mul.log2().floor() as u8 + 1
    }

    /// How much DR against slams, collisions and swung melee weapons can this provide?
    /// note: does not stack with armour DR
    /// This is a compromise for "exoskeleton" and "powered armour" being separated only by whether it has any DR at all,
    /// while the armour design rules rarely expect 0 DR.
    /// TODO: Think about how to make armour over an exoskeleton frame weigh half as much?
    pub fn frame_dr(&self, occupant_size: i8, tl: TechLevel) -> usize {
        ((self.frame_basic_lift(occupant_size) / 60.0).sqrt() * tl.level() as f32) as usize
    }

    pub fn validate(&self, occupant_size: i8, tl: TechLevel, is_rigid: bool) -> Result<()> {
        match *self {
            Self::None => Ok(()),
            Self::ExoskeletonFrame { strength_bonus, .. } => {
                let max_st = self.maximum_strength_bonus(occupant_size, tl);
                if strength_bonus > max_st {
                    bail!(
                        "Exoskeleton Frame has strength bonus {}, but max strength bonus for SM{} and TL{} is {}",
                        strength_bonus,
                        occupant_size,
                        tl,
                        max_st,
                    );
                } else {
                    Ok(())
                }
            }
            Self::CombatWalker { strength_bonus, .. } => {
                let max_st = self.maximum_strength_bonus(occupant_size, tl);
                if strength_bonus > max_st {
                    bail!(
                        "Combat Walker has strength bonus {}, but max strength bonus for SM{} and TL{} is {}",
                        strength_bonus,
                        occupant_size,
                        tl,
                        max_st,
                    );
                } else {
                    Ok(())
                }
            }
            Self::CyberSuit { strength_bonus, .. } => {
                let max_st = self.maximum_strength_bonus(occupant_size, tl);
                if is_rigid {
                    bail!("CyberSuits cannot be rigid");
                }
                if strength_bonus > max_st {
                    bail!(
                        "CyberSuit has strength bonus {}, but max strength bonus for SM{} and TL{} is {}",
                        strength_bonus,
                        occupant_size,
                        tl,
                        max_st,
                    );
                } else {
                    Ok(())
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn typical_weight_matches_gurb() {
        assert_eq!(typical_weight(typical_strength(0)), 150.0)
    }

    #[test]
    fn super_jump_matches_ultra_tech() {
        let tl9 = "9".parse().unwrap();
        let tl10 = "10".parse().unwrap();
        let tl11 = "11".parse().unwrap();
        let tl12: TechLevel = "12".parse().unwrap();

        // Ranger Exoskeleton/TL10, ST bonus 12, Super Jump 2, weight 50lbs
        let ranger = Exoskeleton::ExoskeletonFrame {
            strength_bonus: 13,
            civilian: false,
        };
        let ranger_weight = 50.0 - ranger.frame_weight(0, tl10);
        assert_eq!(ranger.frame_super_jump(0, tl10, ranger_weight), 2);

        // TL9 Powered Combat Walkers can't jump
        let combat_walker = Exoskeleton::CombatWalker {
            strength_bonus: 20,
            civilian: false,
        };
        let combat_walker_weight = 800.0 - combat_walker.frame_weight(0, tl9);
        assert_eq!(
            combat_walker.frame_can_jump(0, tl9, combat_walker_weight),
            false
        );

        let powered_combat_armor = Exoskeleton::ExoskeletonFrame {
            strength_bonus: 10,
            civilian: false,
        };
        let powered_combat_armor_weight = 150.0 - powered_combat_armor.frame_weight(0, tl9);
        assert_eq!(
            powered_combat_armor.frame_super_jump(0, tl9, powered_combat_armor_weight),
            1
        );

        // Heavy Battlesuit/TL10, ST bonus +20, Super Jump 1, weight 480-500lbs
        let heavy_battlesuit = Exoskeleton::ExoskeletonFrame {
            strength_bonus: 16,
            civilian: false,
        };
        let heavy_battlesuit_weight = 480.0 - heavy_battlesuit.frame_weight(0, tl10);
        assert_eq!(
            heavy_battlesuit.frame_super_jump(0, tl10, heavy_battlesuit_weight),
            1
        );

        // Scout Battlesuit/TL10, ST bonus +16, Super Jump 3, weight 480-500 ??? "Its exoskeleton is less strong but faster???"
        // Cybersuit/TL11, ST bonus +5, Super Jump 1, weighs 30lbs
        let cybersuit = Exoskeleton::CyberSuit {
            strength_bonus: 5,
            civilian: true,
        };
        let cybersuit_weight = 30.0 - cybersuit.frame_weight(0, tl11);
        assert_eq!(cybersuit.frame_super_jump(0, tl11, cybersuit_weight), 1);
        // Military Cybersuit/TL11, ST bonus +10, Super Jump 2, weight 50lbs
        let mil_cybersuit = Exoskeleton::CyberSuit {
            strength_bonus: 5,
            civilian: false,
        };
        let mil_cybersuit_weight = 50.0 - mil_cybersuit.frame_weight(0, tl11);
        assert_eq!(
            mil_cybersuit.frame_super_jump(0, tl11, mil_cybersuit_weight),
            1
        );

        // Dreadnought Battlesuit/TL11, ST bonus +30, Super Jump 3, weight 500lbs
        // This formula actually gives Super Jump 2, but given lift_mul is ~3,
        // giving jump distance x8 stretches disbelief.
        // let dread = Exoskeleton::ExoskeletonFrame { strength_bonus: 30, civilian: false };
        // let dread_weight = 500.0 - dread.frame_weight(0, tl11);
        // assert_eq!(dread.frame_super_jump(0, tl11, dread_weight), 3);

        // Nanosuit/TL12, ST bonus +10, Super Jump 2, weight 20lbs
        let nano = Exoskeleton::CyberSuit {
            strength_bonus: 10,
            civilian: false,
        };
        let nano_weight = 20.0 - nano.frame_weight(0, tl12);
        assert_eq!(nano.frame_super_jump(0, tl12, nano_weight), 2);
        // Warsuit/TL12, ST bonus +40, Super Jump 4, weight 500lbs
        // This formula actually gives Super Jump 3, but given lift_mul is 4.4,
        // rounding it up to 8 is already extremely generous.
        // let war = Exoskeleton::ExoskeletonFrame { strength_bonus: 40, civilian: false };
        // let war_weight = 500.0 - war.frame_weight(0, tl12);
        // assert_eq!(war.frame_super_jump(0, tl12, war_weight), 4);
    }
}
