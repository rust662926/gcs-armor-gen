// An Accessory must cover:

// Can I roll transparency into accessories? As equipment modifier with feature
// * DR multiplier -1x for visible-light lasers

use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::tech_level::TechLevel;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub(crate) enum CostModifier {
    None,
    Additive(f32),
    BaseMultiple(f32),
    SurfaceAreaMultiple(f32),
}

impl CostModifier {
    fn is_none(&self) -> bool {
        *self == Self::None
    }
}

impl std::default::Default for CostModifier {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub(crate) enum WeightModifier {
    None,
    Additive(f32),
    BaseMultiple(f32),
    SurfaceAreaMultiple(f32),
}

impl WeightModifier {
    fn is_none(&self) -> bool {
        *self == Self::None
    }
}

impl std::default::Default for WeightModifier {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub(crate) enum RigidityRestriction {
    None,
    RigidOnly,
    FlexibleOnly,
}

impl std::fmt::Display for RigidityRestriction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{}",
            match self {
                Self::None => "None",
                Self::RigidOnly => "Rigid",
                Self::FlexibleOnly => "Flexible",
            }
        )
    }
}

impl RigidityRestriction {
    fn is_none(&self) -> bool {
        *self == Self::None
    }
}

impl std::default::Default for RigidityRestriction {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
enum RequiresStatement {
    None,
    Just(String),
    AllOf(Vec<RequiresStatement>),
    OneOf(Vec<RequiresStatement>),
    NoneOf(Vec<String>),
}

impl RequiresStatement {
    fn is_none(&self) -> bool {
        *self == Self::None
    }

    fn check_allowed(&self, present: &[String]) -> Result<(), Vec<String>> {
        match self {
            Self::None => Ok(()),
            Self::Just(required) => match present.contains(required) {
                true => Ok(()),
                false => Err(vec![format!("Required '{}' is not present", required)]),
            },
            Self::NoneOf(all_forbidden) => {
                let reasons = all_forbidden
                    .iter()
                    .filter_map(|forbidden| match present.contains(forbidden) {
                        true => Some(format!("Forbidden '{}' is present", forbidden)),
                        false => None,
                    })
                    .collect::<Vec<_>>();
                match reasons.is_empty() {
                    true => Ok(()),
                    false => Err(reasons),
                }
            }
            Self::OneOf(statements) => {
                let results = statements
                    .iter()
                    .map(|s| s.check_allowed(present))
                    .collect::<Vec<Result<_, _>>>();
                match results.iter().any(Result::is_ok) {
                    true => Ok(()),
                    false => Err(results
                        .into_iter()
                        .filter_map(Result::err)
                        .flatten()
                        .collect::<Vec<_>>()),
                }
            }
            Self::AllOf(statements) => {
                let results = statements
                    .iter()
                    .map(|s| s.check_allowed(present))
                    .collect::<Vec<Result<_, _>>>();
                match results.iter().all(Result::is_ok) {
                    true => Ok(()),
                    false => Err(results
                        .into_iter()
                        .filter_map(Result::err)
                        .flatten()
                        .collect::<Vec<_>>()),
                }
            }
        }
    }
}

impl std::default::Default for RequiresStatement {
    fn default() -> Self {
        Self::None
    }
}

// Accessories will be output as equipment modifiers (with features)
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Accessory {
    /// The tech level this accessory is available at
    tech_level: TechLevel,

    /// The name of the accessory
    name: String,

    /// A description of what this accessory is
    description: String,

    /// Where to look up this accessory
    reference: String,

    /// How the cost of the armor is affected
    #[serde(
        skip_serializing_if = "CostModifier::is_none",
        default = "Default::default"
    )]
    cost: CostModifier,

    /// How the weight of the armor is affected
    #[serde(
        skip_serializing_if = "WeightModifier::is_none",
        default = "Default::default"
    )]
    weight: WeightModifier,

    /// If any, the list of materials this accessory is restricted to
    #[serde(skip_serializing_if = "Vec::is_empty", default = "Vec::new")]
    material_restriction: Vec<String>,

    /// Whether this accessory is restricted to rigid or flexible armor
    #[serde(
        skip_serializing_if = "RigidityRestriction::is_none",
        default = "Default::default"
    )]
    rigidity_restriction: RigidityRestriction,

    /// Every hit location that must be part of the armor to allow this accessory
    #[serde(skip_serializing_if = "Vec::is_empty", default = "Default::default")]
    hit_location_restriction: Vec<String>,

    /// Whether this accessory needs a certain amount of DR
    #[serde(skip_serializing_if = "Option::is_none")]
    minimum_dr: Option<u32>,

    /// Conditional multipliers of DR
    /// e.g. [("visible light", -1)] for something that doesn't protect against visible light
    #[serde(
        skip_serializing_if = "HashMap::is_empty",
        default = "Default::default"
    )]
    dr_multipliers: HashMap<String, f32>,

    /// Body must be at least this fraction covered
    #[serde(skip_serializing_if = "Option::is_none")]
    minimum_coverage: Option<f32>,

    /// Other accessories that this may require
    #[serde(
        skip_serializing_if = "RequiresStatement::is_none",
        default = "Default::default"
    )]
    requires_accessories: RequiresStatement,
}

impl Accessory {
    #[allow(clippy::too_many_arguments)]
    pub fn is_eligible(
        &self,
        tech_level: &TechLevel,
        material: &str,
        is_rigid: bool,
        hit_locations: &[String],
        dr: u32,
        coverage: f32,
        accessories: &[String],
    ) -> Result<&Self, Vec<String>> {
        let mut reasons = Vec::<String>::new();
        if *tech_level < self.tech_level {
            reasons.push(format!(
                "Tech Level {} is insufficient to meet {}",
                tech_level, self.tech_level
            ));
        }

        if !self.material_restriction.is_empty()
            && !self.material_restriction.iter().any(|r| **r == *material)
        {
            reasons.push(format!(
                "Armor's material {} not found in required materials {:?}",
                material, self.material_restriction
            ));
        }

        match self.rigidity_restriction {
            RigidityRestriction::None => {}
            RigidityRestriction::FlexibleOnly => {
                if is_rigid {
                    reasons.push("Armor is rigid, not flexible".into());
                }
            }
            RigidityRestriction::RigidOnly => {
                if !is_rigid {
                    reasons.push("Armor is flexible, not rigid".into());
                }
            }
        }

        for location in self.hit_location_restriction.iter() {
            if !hit_locations.contains(location) {
                reasons.push(format!(
                    "Required hit location {} not in {:?}",
                    location, hit_locations
                ));
            }
        }

        if let Some(min_dr) = self.minimum_dr {
            if dr < min_dr {
                reasons.push(format!(
                    "Armor's DR {} is less than required DR {}",
                    dr, min_dr
                ));
            }
        }

        if let Some(min_coverage) = self.minimum_coverage {
            if coverage < min_coverage {
                reasons.push(format!(
                    "Armor's coverage {} is less than required coverage {}",
                    coverage, min_coverage
                ));
            }
        }

        if let Err(mut failure_messages) = self.requires_accessories.check_allowed(accessories) {
            reasons.append(&mut failure_messages);
        }
        match reasons.is_empty() {
            true => Ok(self),
            false => Err(reasons),
        }
    }

    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub(crate) fn reference(&self) -> &str {
        self.reference.as_ref()
    }

    pub(crate) fn description(&self) -> &str {
        self.description.as_ref()
    }

    pub(crate) fn cost(&self) -> &CostModifier {
        &self.cost
    }

    pub(crate) fn weight(&self) -> &WeightModifier {
        &self.weight
    }

    pub(crate) fn dr_multipliers(&self) -> &HashMap<String, f32> {
        &self.dr_multipliers
    }

    pub(crate) fn tech_level(&self) -> TechLevel {
        self.tech_level
    }
}

impl Default for Accessory {
    fn default() -> Self {
        Self {
            tech_level: "0".parse().unwrap(),
            name: String::new(),
            description: String::new(),
            reference: String::new(),
            cost: CostModifier::None,
            weight: WeightModifier::None,
            material_restriction: vec![],
            rigidity_restriction: RigidityRestriction::None,
            hit_location_restriction: vec![],
            minimum_dr: Option::None,
            dr_multipliers: HashMap::new(),
            minimum_coverage: Option::None,
            requires_accessories: RequiresStatement::None,
        }
    }
}
