use serde::{Deserialize, Serialize};

use crate::size_modifier;

/// Armor shape, what a piece of armor covers and how much area that is
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ArmorShape {
    /// Name of this entry
    name: String,
    /// Square feet required to cover this part of the average human body
    area: f32,
    /// Hit locations that this covers
    /// These locations should correspond to hit locations in a character sheet's Body Type in GCS.
    covered_locations: Vec<String>,
}

impl ArmorShape {
    pub fn percentage_covered(&self) -> f32 {
        self.area / 21.35
    }

    pub fn square_footage(&self, size_modifier: i8) -> f32 {
        self.area * size_modifier::area_factor(size_modifier)
    }

    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub fn covered_locations(&self) -> &[String] {
        self.covered_locations.as_ref()
    }
}
