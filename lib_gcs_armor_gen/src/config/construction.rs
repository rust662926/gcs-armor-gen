use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug, Clone)]
enum Rigidity {
    Rigid,
    RigidIfThick,
    Flexible,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Construction {
    /// Name of the construction type
    name: String,

    /// Weight multiplied by this factor if this construction is used
    weight_mul: f32,

    /// Cost multiplied by this factor if this construction is used
    cost_mul: f32,

    /// Minimum DR that this construction can be used with
    min_dr: u32,

    /// Notes for this particular kind of construction
    #[serde(skip_serializing_if = "Vec::is_empty", default = "Default::default")]
    notes: Vec<String>,

    /// Additional conditional multipliers of DR, e.g. [("Not crushing", -0.5)]
    #[serde(
        skip_serializing_if = "HashMap::is_empty",
        default = "Default::default"
    )]
    dr_multipliers: HashMap<String, f32>,

    /// Additive or subtractive modifiers of DR, e.g. [("Crushing", -1)]
    #[serde(
        skip_serializing_if = "HashMap::is_empty",
        default = "Default::default"
    )]
    dr_additions: HashMap<String, i32>,

    /// Construction can only be used with certain hit locations
    #[serde(skip_serializing_if = "Vec::is_empty", default = "Default::default")]
    restricted_to_locations: Vec<String>,

    /// Whether this construction is rigid or flexible
    rigidity: Rigidity,
}

impl Construction {
    pub fn is_rigid(&self, dr: u32, max_dr: u32) -> bool {
        match self.rigidity {
            Rigidity::Rigid => true,
            Rigidity::RigidIfThick => dr as f32 / max_dr as f32 >= 0.25,
            Rigidity::Flexible => false,
        }
    }

    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub(crate) fn restricted_to_locations(&self) -> &[String] {
        self.restricted_to_locations.as_ref()
    }

    pub(crate) fn min_dr(&self) -> u32 {
        self.min_dr
    }

    pub(crate) fn weight_mul(&self) -> f32 {
        self.weight_mul
    }

    pub(crate) fn cost_mul(&self) -> f32 {
        self.cost_mul
    }

    pub(crate) fn dr_multipliers(&self) -> &HashMap<String, f32> {
        &self.dr_multipliers
    }

    pub(crate) fn dr_additions(&self) -> &HashMap<String, i32> {
        &self.dr_additions
    }

    pub fn is_eligible(
        &self,
        used_locations: &[String],
        allowed_construction_types: &[String],
    ) -> Result<(), String> {
        let mut ineligible_reasons: Vec<String> = vec![];
        if !self.restricted_to_locations.is_empty() {
            let mut ineligible_locations: Vec<String> = vec![];
            for used in used_locations {
                if !self.restricted_to_locations.contains(used) {
                    ineligible_locations.push(used.clone())
                }
            }
            if !ineligible_locations.is_empty() {
                ineligible_reasons.push(format!(
                    "The only allowed hit locations are {}, can't be used with {}",
                    self.restricted_to_locations.join(", "),
                    ineligible_locations.join(", ")
                ));
            }
        }

        if !allowed_construction_types.contains(&self.name) {
            ineligible_reasons.push(format!(
                "The only constructions allowed by the chosen material are {}",
                allowed_construction_types.join(", ")
            ));
        }
        match ineligible_reasons.is_empty() {
            true => Ok(()),
            false => Err(ineligible_reasons.join("; ")),
        }
    }
}

impl Default for Construction {
    fn default() -> Self {
        Self {
            name: String::new(),
            weight_mul: 1.0,
            cost_mul: 1.0,
            min_dr: 0,
            notes: vec![],
            dr_multipliers: HashMap::new(),
            dr_additions: HashMap::new(),
            restricted_to_locations: vec![],
            rigidity: Rigidity::Rigid,
        }
    }
}
