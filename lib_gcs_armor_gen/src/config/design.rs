use serde::{Deserialize, Serialize};

use crate::concealment::Concealment;
use crate::config::exoskeleton::Exoskeleton;
use crate::tech_level::TechLevel;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Design {
    /// The Tech Level that this armor is built at
    pub tech_level: TechLevel,

    /// The name given to the armor
    pub name: String,

    // Name of the armor shape
    pub shape: String,

    /// Size Modifier, as the GURPS definition of a Size Modifier, an exponential scale with human size as 0
    pub size_modifier: i8,

    /// Name of the material, if multiple materials with this name exist, take the one with the highest tech level
    pub material: String,

    /// Name of the construction
    pub construction: String,

    pub dr: u32,

    #[serde(default = "Default::default")]
    pub accessories: Vec<String>,

    /// Legality class, as GURPS Basic Set 267 (ranges from 0 to 4)
    pub legality_class: u8,

    /// Whether this armor is designed to be concealed, and as what
    #[serde(
        skip_serializing_if = "Concealment::is_none",
        default = "Default::default"
    )]
    pub concealment: Concealment,

    /// Armour type, for powered armour
    #[serde(
        skip_serializing_if = "Exoskeleton::is_none",
        default = "Default::default"
    )]
    pub exoskeleton: Exoskeleton,
}

impl Design {
    pub fn tech_level(&self) -> TechLevel {
        self.tech_level
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn shape(&self) -> &str {
        &self.shape
    }

    pub fn size_modifier(&self) -> i8 {
        self.size_modifier
    }

    pub fn material(&self) -> &str {
        &self.material
    }

    pub fn construction(&self) -> &str {
        &self.construction
    }

    pub fn dr(&self) -> u32 {
        self.dr
    }

    pub fn accessories(&self) -> &[String] {
        &self.accessories
    }

    pub fn legality_class(&self) -> u8 {
        self.legality_class
    }

    pub fn concealment(&self) -> &Concealment {
        &self.concealment
    }
}
