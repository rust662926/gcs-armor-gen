// Size Modifier, i.e. GURPS' Size Modifier (see GURPS Basic Set p.19)

pub fn length_factor(value: i8) -> f32 {
    10.0f32.powf(value as f32 / 6.0)
}

pub(crate) fn area_factor(value: i8) -> f32 {
    10.0f32.powf(value as f32 / 3.0)
}

// pub(crate) fn volume_factor(value: i8) -> f32 {
//     10.0f32.powf(value as f32/2.0)
// }

/// calculates the size modifier for a given length in yds
pub fn size_modifier_from_length(length: f32) -> i8 {
    // formula from https://gurb3d6.blogspot.com/2016/09/power-armor-powered-up.html
    // I've tested it and it gives correct results even if some of the numbers are a mystery to me
    (6f32 * length.log10() - 2f32).round() as i8
}
