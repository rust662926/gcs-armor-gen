#!/usr/bin/bash -eu

mkdir -p export/cli
cargo build -r --bin gcs_armor_gen
cargo install --path gcs_armor_gen --target-dir export/cli
