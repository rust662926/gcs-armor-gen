#!/usr/bin/bash -eu

cargo build --lib
cargo build -r --lib
mkdir -p export/linux
godot --headless --path gdui --export-release Linux/X11 ../export/gcs_armor_gen_gdui_linux.zip
