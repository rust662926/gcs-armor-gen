#!/usr/bin/bash -eu

cargo build --lib
cargo build -r --lib --target x86_64-pc-windows-gnu
mkdir -p export/windows
godot --headless --path gdui --export-release "Windows Desktop" ../export/gcs_armor_gen_gdui_windows.zip
