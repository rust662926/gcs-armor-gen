GCS Armor Generator
===

This is a project developing tools for generating armor compatible with
[GCS](gurpscharactersheet.com).

This project has two build targets:
* a command-line application, **gcs_armor_gen**
* a godot-based graphical application, **gdui**


It is derived from David Pulver's armor design rules, as described in each of:
* Low-Tech Armor Design from
  [Pyramid 3-52: Low Tech II](https://warehouse23.com/products/pyramid-number-3-slash-52-low-tech-ii)
* Cutting-edge Armor Design from
  [Pyramid 3-85: Cutting Edge](https://warehouse23.com/products/pyramid-number-3-slash-85-cutting-edge)
* Ultra-Tech Armor Design from
  [Pyramid 3-96: Tech and Toys IV](https://warehouse23.com/products/pyramid-number-3-slash-96-tech-and-toys-iv)


How to build
===

### gcs_armor_gen

```
# build the program
cargo build -r --bin gcs_armor_gen
# install the program (defaults to $HOME/.cargo/bin)
cargo install --path gcs_armor_gen
```

### gdui

install godot (https://github.com/godotengine/godot/releases/download/4.2.1-stable/Godot_v4.2.1-stable_linux.x86_64.zip)

install the godot export templates (https://github.com/godotengine/godot/releases/download/4.2.1-stable/Godot_v4.2.1-stable_export_templates.tpz)

#### Linux

```
# First, we need to build the gdlib library, which gdui requires
# This command creates the file target/release/libgdlib.so
cargo build -r --lib

# Ensure the export directory is created
mkdir -p export/linux

# Create the release
godot --headless --path gdui --export-release Linux/X11 ../export/linux/gcs_armor_gen.x86_64
```

### gdui for windows
I don't use Windows often, so I can't provide help with building this natively for Windows.
It'll probably be easier than these instructions on how to cross-compile for Windows.

```
# Install some dependencies
apt install -y osslsigncode gcc-mingw-w64-x86-64

# Add the windows cross-toolchain
rustup target add x86_64-pc-windows-gnu

# Build the library for Windows
cargo build -r --lib --target x86_64-pc-windows-gnu

# Ensure the export directory is created
mkdir -p export/windows

# Create the release
godot --headless --path gdui --export-release "Windows Desktop" ../export/windows/gcs_armor_gen.exe
```

FAQ
---

### What is this license?

This tool is not provided under an Open Source License.
It is instead provided with a non-commercial license,
the [AFPL](https://tldrlegal.com/license/aladdin-free-public-license#summary),
and available to all under personal use.

Unlike its creators, whose intent was to force commercial users to pay
for development, no dual-licensing will be provided.

This is because the project is developed as a hobby and a preference
that work done in my own time remains forever non-commercial, but also
it was developed with intent to use with GURPS, and if any GURPS specific
behaviour is included the tool MUST be non-commercial.


Game aid disclaimer
---

GURPS is a trademark of Steve Jackson Games, and its rules
and art are copyrighted by Steve Jackson Games. All rights are
reserved by Steve Jackson Games.
This game aid is the original creation of Jonathan Maw and is released
for free distribution, and not for resale, under the permissions granted
in the <a href="http://www.sjgames.com/general/online_policy.html">Steve
Jackson Games Online Policy</a>.

Notes for developers
===

To build a new docker image:
```
docker login registry.gitlab.com   # Authenticate with Gitlab to allow pushing to container registry
docker build -t registry.gitlab.com/rust662926/gcs-armor-gen -f Dockerfile  # Create an image with a name that gitlab accepts
docker push registry.gitlab.com/rust662926/gcs-armor-gen  # Upload the new image
```
