FROM debian

RUN apt update
RUN apt -y install \
        build-essential \
        curl \
        osslsigncode \
        gcc-mingw-w64-x86-64 \
        unzip \
        wget \
        zip

# Get godot4 (because only godot3 is in apt)
RUN wget https://github.com/godotengine/godot/releases/download/4.2.1-stable/Godot_v4.2.1-stable_linux.x86_64.zip
RUN unzip Godot_v4.2.1-stable_linux.x86_64.zip
RUN mv Godot_v4.2.1-stable_linux.x86_64 usr/bin/godot

# Download the godot export templates
RUN wget https://github.com/godotengine/godot/releases/download/4.2.1-stable/Godot_v4.2.1-stable_export_templates.tpz
RUN unzip Godot_v4.2.1-stable_export_templates.tpz
RUN mkdir -p /root/.local/share/godot/export_templates
RUN mv templates /root/.local/share/godot/export_templates/4.2.1.stable

# install rust and the cross-windows target
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -q -y -t x86_64-pc-windows-gnu
