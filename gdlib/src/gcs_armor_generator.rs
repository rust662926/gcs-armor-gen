use std::fs::File;
use std::io::{BufReader, Read, Write};
use std::path::PathBuf;

use color_eyre::eyre::{OptionExt, Result, WrapErr};
use godot::engine::global::{Error, Key};
use godot::engine::{
    IPanelContainer, InputEvent, InputEventKey, MenuButton, PanelContainer, TabContainer, TextEdit,
};
use godot::prelude::*;

use lib_gcs_armor_gen::armor_design;
use lib_gcs_armor_gen::config::design::Design;
use lib_gcs_armor_gen::config::Config;
use lib_gcs_armor_gen::gcs::{GcsEquipmentItem, GcsEquipmentLib};

use crate::armor_design_view::ArmorDesignView;

// use crate::armor_design_view::ArmorDesignView;

/// Transforms yaml config into GCS equipment library json
#[derive(GodotClass)]
#[class(tool, init, base=PanelContainer)]
struct GcsArmorGenerator {
    base: Base<PanelContainer>,

    /// The config used to generate armor
    /// This should be a NodePath to a TextEdit
    #[export]
    config: NodePath,
    /// The generated armor, as a GCS equipment library
    /// This should be a NodePath to a TextEdit
    #[export]
    gcs_library: NodePath,
    /// Log for any messages generated
    /// This should be a NodePath to a TextEdit
    #[export]
    log: NodePath,
    /// The path to the ConfigFilePath TextEdit
    #[export]
    config_file_path: NodePath,

    /// The path to a container to put armor designs in
    #[export]
    armor_designs_container: NodePath,

    /// The path to the MenuButton that is used to select a design to edit
    #[export]
    edit_designs_button: NodePath,
}

#[godot_api]
impl IPanelContainer for GcsArmorGenerator {
    fn ready(&mut self) {
        if !self.edit_designs_button.is_empty() {
            let callable = Callable::from_object_method(&self.base_mut(), "extract_design");
            self.base()
                .try_get_node_as::<MenuButton>(self.edit_designs_button.clone())
                .unwrap()
                .get_popup()
                .unwrap()
                .connect("id_pressed".into(), callable);
        }
    }
}

#[godot_api]
impl GcsArmorGenerator {
    fn get_textedit_text(&self, path: NodePath) -> GString {
        self.base().get_node_as::<TextEdit>(path).get_text()
    }

    fn set_textedit_text(&mut self, path: NodePath, text: GString) {
        self.base().get_node_as::<TextEdit>(path).set_text(text)
    }

    fn get_container_node(&self) -> Gd<TabContainer> {
        self.base()
            .get_node_as::<TabContainer>(self.get_armor_designs_container())
    }

    #[func]
    fn new_armor_design(&mut self) {
        match self.try_new_armor_design() {
            Ok(()) => {}
            Err(msg) => self.append_log(msg),
        }
    }

    fn try_new_armor_design(&mut self) -> Result<()> {
        let mut container_node = self.get_container_node();
        let config_node_path = format!("{}/{}", self.base().get_path(), self.get_config());
        let new_design_node = ArmorDesignView::new_design(config_node_path.into());

        container_node.add_child(new_design_node.upcast());
        Ok(())
    }

    #[func]
    fn generate_designs_buttons(&mut self) {
        match self.try_generate_designs_buttons() {
            Ok(()) => {}
            Err(msg) => self.append_log(msg),
        }
    }

    fn try_generate_designs_buttons(&mut self) -> Result<()> {
        let config_text = self.get_textedit_text(self.get_config());
        let config = Self::load_config(config_text.to_string().as_str())?;
        let mut popup_node = self
            .base()
            .try_get_node_as::<MenuButton>(self.edit_designs_button.clone())
            .ok_or_eyre(format!(
                "Failed to get menu button '{}'",
                self.edit_designs_button
            ))?
            .get_popup()
            .ok_or_eyre("Failed to get popup of menu button")?;
        popup_node.clear();
        for (idx, design) in config.designs().iter().enumerate() {
            popup_node
                .add_item_ex(design.name().to_godot())
                .id(idx as i32)
                .done()
        }
        if popup_node.get_item_count() == 0 {
            popup_node.add_item("There are no designs to edit".into());
            popup_node.set_item_disabled(0, true);
        }
        Ok(())
    }

    #[func]
    fn extract_design(&mut self, id: i32) {
        match self.try_extract_design(id) {
            Ok(()) => {}
            Err(msg) => self.append_log(msg),
        }
    }

    fn try_extract_design(&mut self, id: i32) -> Result<()> {
        let config_text = self.get_textedit_text(self.get_config());
        let mut config = Self::load_config(config_text.to_string().as_str())?;
        // pull out the chosen design
        let mut container_node = self.get_container_node();
        let design = config.designs_mut().remove(id as usize);

        // create the new design
        let config_node_path = format!("{}/{}", self.base().get_path(), self.get_config());
        let design_node = ArmorDesignView::try_from_design(design, config_node_path.into())?;
        container_node.add_child(design_node.upcast());

        // write back the changed config
        self.set_textedit_text(self.get_config(), serde_yaml::to_string(&config)?.into());

        Ok(())
    }

    #[func]
    fn generate(&mut self) {
        match Self::gcs_library_from_config(
            self.get_textedit_text(self.get_config())
                .to_string()
                .as_str(),
        ) {
            Ok(output) => {
                self.set_textedit_text(self.get_gcs_library(), output.into_godot());
            }
            Err(e) => {
                self.append_log(e);
            }
        }
    }

    #[func]
    fn load_config_file(&mut self, path: GString) {
        match Self::try_load_config_file(self, path) {
            Ok(text) => {
                self.set_textedit_text(self.get_config(), text);
            }
            Err(msg) => {
                self.append_log(msg);
            }
        }
    }

    #[func]
    fn load_config_files(&mut self, paths: PackedStringArray) {
        for path in paths.as_slice() {
            self.load_config_file(path.clone())
        }
    }

    #[func]
    fn save_gcs_lib(&mut self, path: GString) {
        match Self::try_save_gcs_lib(self, &path) {
            Ok(()) => self.append_log(format!("Successfully saved GCS library to {}", &path)),
            Err(msg) => self.append_log(msg),
        }
    }

    fn try_save_gcs_lib(&mut self, path: &GString) -> Result<()> {
        let lib = self.get_textedit_text(self.get_gcs_library()).to_string();
        File::create(path.to_string())?.write_all(lib.as_bytes())?;
        Ok(())
    }

    #[func]
    fn preview_gcs_lib(&mut self) {
        match Self::try_preview_gcs_lib(self) {
            Ok(()) => {}
            Err(msg) => self.append_log(msg),
        }
    }

    fn try_preview_gcs_lib(&mut self) -> Result<()> {
        let mut preview_path: PathBuf = std::env::temp_dir();
        preview_path.push("gcs_armor_gen_preview.eqp");
        let preview_path = preview_path.to_string_lossy().to_string().to_godot();
        self.try_save_gcs_lib(&preview_path)?;

        let mut os = godot::engine::Os::singleton();

        let result = os.shell_open(preview_path);
        if result == Error::ERR_FILE_NOT_FOUND {
            self.append_log("Failed to open preview. Possibly you haven't got a handler configured for .eqp files?");
        }
        Ok(())
    }

    fn append_log(&mut self, text: impl std::fmt::Debug) {
        let log_text = self.get_textedit_text(self.get_log());
        self.set_textedit_text(
            self.get_log(),
            format!("{}\n{:#?}", log_text, text).into_godot(),
        );
    }

    fn try_load_config_file(&mut self, path: GString) -> Result<GString> {
        // Parse existing config into a struct
        let old_config_text = self.get_textedit_text(self.get_config());
        let mut config = Self::load_config(old_config_text.to_string().as_str())
            .wrap_err_with(|| "Failed to load existing config")?;

        let load_path = path.to_string().replace('\n', "");

        let mut text = String::new();
        BufReader::new(File::open(&load_path)?)
            .read_to_string(&mut text)
            .wrap_err_with(|| format!("Failed to read file '{}'", &load_path))?;
        let new_config = Self::load_config(&text).wrap_err_with(|| "Failed to load new config")?;
        config.append(new_config);

        let new_config_text: GString = serde_yaml::to_string(&config)
            .wrap_err_with(|| "Failed to serialise merged config")?
            .to_godot();
        Ok(new_config_text)
    }

    #[func]
    fn on_config_file_path_input_event(&mut self, event: Gd<InputEvent>) {
        let event = event.try_cast::<InputEventKey>();
        if event.is_err() {
            return;
        }
        let event = event.unwrap();
        if !event.is_released() {
            return;
        }
        if event.get_keycode() == Key::ENTER {
            let mut conf_path_node = self
                .base()
                .get_node_as::<TextEdit>(self.get_config_file_path());
            let file_path = conf_path_node.get_text();
            self.load_config_file(file_path);
            conf_path_node.set_text("".into());
        }
    }

    fn gcs_library_from_config(config_text: &str) -> Result<String> {
        let config = Self::load_config(config_text)?;
        let loaded_designs = armor_design::load_armor_designs(&config)?;
        let items = loaded_designs
            .into_iter()
            .map(|s| s.into())
            .collect::<Vec<GcsEquipmentItem>>();
        let lib: GcsEquipmentLib = items.into();
        let output = serde_json::to_string_pretty(&lib)?;
        Ok(output)
    }

    fn load_config(config_text: &str) -> Result<Config> {
        let config: Config = serde_yaml::from_str(config_text)
            .or_else(|_| serde_yaml::from_str::<Vec<Design>>(config_text).map(|v| v.into()))?;
        Ok(config)
    }
}
