use godot::prelude::*;

struct ArmorGenExtension;

#[gdextension]
unsafe impl ExtensionLibrary for ArmorGenExtension {}

pub mod armor_design_view;
pub mod gcs_armor_generator;
