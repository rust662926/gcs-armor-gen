use color_eyre::eyre::{eyre, OptionExt, Result};
use godot::engine::global::HorizontalAlignment;
use godot::engine::{
    BoxContainer, Button, CanvasItem, CheckBox, IScrollContainer, Label, MenuButton, OptionButton,
    ScrollContainer, TextEdit, VBoxContainer,
};
use godot::prelude::*;
use lib_gcs_armor_gen::concealment::Concealment;
use lib_gcs_armor_gen::config::armor_shape::ArmorShape;
use lib_gcs_armor_gen::config::construction::Construction;
use lib_gcs_armor_gen::config::exoskeleton::Exoskeleton;
use lib_gcs_armor_gen::config::material::Material;
use lib_gcs_armor_gen::{armor_design, size_modifier};

use lib_gcs_armor_gen::config::design::Design;
use lib_gcs_armor_gen::config::Config;
use lib_gcs_armor_gen::gcs::GcsEquipmentItem;
use lib_gcs_armor_gen::tech_level::TechLevel;

const ARMOR_ICON: &str = "🧥";
const SCENE_PATH: &str = "res://armor_design_view.tscn";

/// Displays an ArmorDesign as a godot scene
#[derive(GodotClass)]
#[class(tool, init, base=ScrollContainer)]
pub(crate) struct ArmorDesignView {
    base: Base<ScrollContainer>,
    config_node_path: NodePath,
    #[export]
    tech_level_node: NodePath,
    #[export]
    name_node: NodePath,
    #[export]
    armor_shape_node: NodePath,
    #[export]
    size_modifier_node: NodePath,
    #[export]
    material_node: NodePath,
    #[export]
    construction_node: NodePath,
    #[export]
    dr_node: NodePath,
    #[export]
    legality_class_node: NodePath,
    #[export]
    concealment_node: NodePath,
    #[export]
    exoskeleton_type_node: NodePath,
    #[export]
    exoskeleton_strength_bonus_label: NodePath,
    #[export]
    exoskeleton_strength_bonus_node: NodePath,
    #[export]
    exoskeleton_civilian_label: NodePath,
    #[export]
    exoskeleton_civilian_node: NodePath,
    #[export]
    accessories_node: NodePath,
    #[export]
    add_accessory_node: NodePath,
    #[export]
    preview_node: NodePath,
}

#[godot_api]
impl IScrollContainer for ArmorDesignView {
    fn ready(&mut self) {
        self.populate_dropdowns();
        self.update_exoskeleton_fields();
        self.update_exoskeleton_type_dropdown();
        self.base_mut()
            .emit_signal("accessories_changed".into(), &[]);
    }
}

#[godot_api]
impl ArmorDesignView {
    #[signal]
    fn accessories_changed();

    fn set_textedit_text(
        root: &Gd<ArmorDesignView>,
        child_path: &NodePath,
        text: String,
    ) -> Result<()> {
        root.try_get_node_as::<TextEdit>(child_path.clone())
            .ok_or_eyre(format!("No TextEdit node found at '{child_path}'"))?
            .set_text(text.into());
        Ok(())
    }

    fn get_textedit_text(&self, child_path: &NodePath) -> Result<String> {
        let text = self
            .base()
            .try_get_node_as::<TextEdit>(child_path.clone())
            .ok_or_eyre(format!("No TextEdit node found at '{child_path}'"))?
            .get_text()
            .into();
        Ok(text)
    }

    fn get_button_text(&self, child_path: &NodePath) -> Result<String> {
        let text = self
            .base()
            .try_get_node_as::<Button>(child_path.clone())
            .ok_or_eyre(format!("No Button node found at '{child_path}'"))?
            .get_text()
            .into();
        Ok(text)
    }

    #[func]
    fn update_exoskeleton_type_dropdown(&mut self) {
        if let Err(result) = self.try_update_exoskeleton_type_dropdown() {
            println!("{}", result)
        }
    }

    fn try_update_exoskeleton_type_dropdown(&mut self) -> Result<()> {
        let mut exoskeleton_type = self
            .base()
            .try_get_node_as::<OptionButton>(self.get_exoskeleton_type_node())
            .ok_or_eyre(format!(
                "No OptionButton node found at {}'",
                self.get_exoskeleton_type_node()
            ))?;
        let tech_level = self.try_get_tech_level()?;
        let item_count = exoskeleton_type.get_item_count();
        let config = self.try_get_config()?;
        let dr = self.try_get_dr()?;
        let material = self.find_material(&config)?;
        let construction = self.find_construction(&config)?;
        let rigid = construction.is_rigid(dr, material.unscaled_max_dr());
        for n in 0..item_count {
            let exoskeleton_type_text = exoskeleton_type.get_item_text(n).to_string();
            if exoskeleton_type_text.as_str() != "None" {
                if tech_level < "9".parse()? {
                    exoskeleton_type.set_item_disabled(n, true);
                    exoskeleton_type
                        .set_item_tooltip(n, "Exoskeletons can't be used before TL9".into());
                    if exoskeleton_type.get_selected() == n {
                        exoskeleton_type.select(0);
                        self.update_exoskeleton_fields();
                    }
                    continue;
                }
                if exoskeleton_type_text.as_str() == "CyberSuit" && rigid {
                    exoskeleton_type.set_item_disabled(n, true);
                    exoskeleton_type.set_item_tooltip(n, "CyberSuits must be flexible".into());
                    if exoskeleton_type.get_selected() == n {
                        exoskeleton_type.select(0);
                        self.update_exoskeleton_fields();
                    }
                    continue;
                }
            }
            exoskeleton_type.set_item_disabled(n, false);
            exoskeleton_type.set_item_tooltip(n, "".into());
        }
        Ok(())
    }

    #[func]
    fn update_exoskeleton_fields(&mut self) {
        let exoskeleton_type = self
            .base()
            .try_get_node_as::<OptionButton>(self.get_exoskeleton_type_node())
            .unwrap()
            .get_text()
            .to_string();
        let fields_visible = !matches!(exoskeleton_type.as_str(), "None");
        for path in [
            self.get_exoskeleton_strength_bonus_label(),
            self.get_exoskeleton_strength_bonus_node(),
            self.get_exoskeleton_civilian_label(),
            self.get_exoskeleton_civilian_node(),
        ] {
            self.base()
                .try_get_node_as::<CanvasItem>(path)
                .unwrap()
                .set_visible(fields_visible);
        }
    }

    #[func]
    fn update_preview(&mut self) {
        let mut preview_node = self
            .base()
            .try_get_node_as::<Label>(self.get_preview_node())
            .unwrap();
        preview_node.set_text(match self.try_update_preview() {
            Ok(text) => text.to_godot(),
            Err(msg) => msg.to_string().to_godot(),
        })
    }

    fn try_update_preview(&mut self) -> Result<String> {
        let config = self.try_get_config()?;
        let design = self.try_to_design()?;
        let loaded_design = armor_design::load_armor_design(&config, &design)?;
        design.exoskeleton.validate(
            loaded_design.size_modifier(),
            loaded_design.tech_level(),
            loaded_design.rigid(),
        )?;
        let gcs_design: GcsEquipmentItem = loaded_design.into();
        let design_text = serde_yaml::to_string(&gcs_design)?;
        Ok(design_text)
    }

    #[func]
    fn sync_name(&mut self) {
        let new_name = self
            .base()
            .get_node_as::<TextEdit>(self.name_node.clone())
            .get_text();
        self.base_mut()
            .set_name(format!("{}{}", ARMOR_ICON, new_name).into());
    }

    fn try_get_dr(&self) -> Result<u32> {
        let dr_text = self.get_textedit_text(&self.dr_node)?;
        let dr: u32 = dr_text.parse()?;
        Ok(dr)
    }

    fn try_get_tech_level(&self) -> Result<TechLevel> {
        let tl_text = self.get_textedit_text(&self.tech_level_node)?;
        let tl: TechLevel = tl_text.parse()?;
        Ok(tl)
    }

    fn find_material<'a>(&'a self, config: &'a Config) -> Result<Material> {
        let tl = self.try_get_tech_level()?;
        let dr = self.try_get_dr()?;
        let exoskeleton = self.try_get_exoskeleton()?;
        let size_modifier = self.get_textedit_text(&self.size_modifier_node)?.parse()?;
        let exoskeleton_size_modifier = exoskeleton.frame_size_modifier(size_modifier, tl);
        let material_node = self.try_get_option_button(&self.material_node)?;
        let material_text = material_node
            .get_item_text(material_node.get_selected_id())
            .to_string();
        let material = config
            .find_material(material_text.as_ref(), &tl, dr, exoskeleton_size_modifier)
            .ok_or_eyre(format!(
                "No material found for name {}, TL {} and DR {}",
                material_text, tl, dr
            ))?;
        Ok(material)
    }

    fn find_construction<'a>(&'a self, config: &'a Config) -> Result<&Construction> {
        let construction_node = self.try_get_option_button(&self.construction_node)?;
        let construction_text = construction_node
            .get_item_text(construction_node.get_selected_id())
            .to_string();
        let constructions = config
            .constructions()
            .iter()
            .filter(|c| c.name() == construction_text)
            .collect::<Vec<_>>();
        let construction = constructions.last().ok_or_eyre(format!(
            "Could not find construction named {construction_text}"
        ))?;
        Ok(construction)
    }

    fn find_armor_shape<'a>(&'a self, config: &'a Config) -> Result<&ArmorShape> {
        let armor_shape_node = self.try_get_option_button(&self.armor_shape_node)?;
        let armor_shape_text = armor_shape_node
            .get_item_text(armor_shape_node.get_selected_id())
            .to_string();
        let armor_shapes = config
            .armor_shapes()
            .iter()
            .filter(|s| s.name() == armor_shape_text)
            .collect::<Vec<_>>();
        let armor_shape = armor_shapes.last().ok_or_eyre(format!(
            "Could not find armor shape named {armor_shape_text}"
        ))?;
        Ok(armor_shape)
    }

    fn try_get_option_button(&self, node_path: &NodePath) -> Result<Gd<OptionButton>> {
        self.base()
            .try_get_node_as::<OptionButton>(node_path.clone())
            .ok_or_eyre(format!("No OptionButton found at '{node_path}'"))
    }

    fn try_get_menu_button(&mut self, node_path: &NodePath) -> Result<Gd<MenuButton>> {
        self.base()
            .try_get_node_as::<MenuButton>(node_path.clone())
            .ok_or_eyre(format!("No MenuButton fount at '{node_path}'"))
    }

    #[func]
    fn populate_armor_shape(&mut self) {
        if let Err(result) = self.try_populate_armor_shape() {
            println!("{}", result)
        }
    }

    fn try_populate_armor_shape(&mut self) -> Result<()> {
        let config = self.try_get_config()?;
        let mut armor_shape_node = self.try_get_option_button(&self.armor_shape_node)?;
        let original_armor_shape =
            armor_shape_node.get_item_text(armor_shape_node.get_selected_id());
        armor_shape_node.clear();
        for (idx, armor_shape) in config.armor_shapes().iter().enumerate() {
            let idx: i32 = idx.try_into()?;
            armor_shape_node
                .add_item_ex(armor_shape.name().into())
                .id(idx)
                .done();
            armor_shape_node
                .set_item_tooltip(idx, serde_yaml::to_string(armor_shape).unwrap().to_godot());
            if original_armor_shape == armor_shape.name().into() {
                armor_shape_node.select(idx);
            }
        }
        Ok(())
    }

    #[func]
    fn populate_material(&mut self) {
        if let Err(result) = self.try_populate_material() {
            println!("{}", result)
        }
    }

    fn try_get_materials(&mut self) -> Result<Vec<Material>> {
        let config = self.try_get_config()?;
        let tl = self.try_get_tech_level()?;
        let size_modifier = self.get_textedit_text(&self.size_modifier_node)?.parse()?;
        let exoskeleton = self.try_get_exoskeleton()?;
        let exoskeleton_size_modifier = exoskeleton.frame_size_modifier(size_modifier, tl);
        let materials = match config.options().max_dr_scales_with_size() {
            true => Material::scale_max_dr(
                config.materials().to_vec(),
                size_modifier::length_factor(exoskeleton_size_modifier),
            ),
            false => config.materials().to_vec(),
        };
        Ok(materials)
    }

    fn try_get_material_eligible(&mut self, material: &Material) -> Result<()> {
        let dr = self.try_get_dr()?;
        let tl = self.try_get_tech_level()?;
        material.is_eligible(&tl, dr).map_err(|e| eyre!(e))
    }

    // get everything to fill in a dropdown.
    // The tuple is material name, disabled, and tooltip
    fn get_material_dropdown_values(&mut self) -> Result<Vec<(String, bool, String)>> {
        let config = self.try_get_config()?;
        match self.try_get_materials() {
            Ok(materials) => Ok(materials
                .into_iter()
                .map(|m| match self.try_get_material_eligible(&m) {
                    Ok(()) => (
                        m.name().to_string(),
                        false,
                        serde_yaml::to_string(&m).unwrap(),
                    ),
                    Err(msg) => (m.name().to_string(), true, msg.to_string()),
                })
                .collect::<Vec<_>>()),
            Err(msg) => Ok(config
                .materials()
                .iter()
                .map(|m| (m.name().to_string(), true, msg.to_string()))
                .collect::<Vec<_>>()),
        }
    }

    fn try_populate_material(&mut self) -> Result<()> {
        let mut material_node = self.try_get_option_button(&self.material_node)?;
        let original_material = material_node.get_item_text(material_node.get_selected_id());
        material_node.clear();
        for (idx, (name, disabled, tooltip)) in
            self.get_material_dropdown_values()?.iter().enumerate()
        {
            let idx: i32 = idx.try_into()?;
            material_node.add_item_ex(name.into()).id(idx).done();
            material_node.set_item_disabled(idx, *disabled);
            material_node.set_item_tooltip(idx, tooltip.to_godot());
            if original_material == name.to_godot() && !*disabled {
                material_node.select(idx);
            }
        }
        Ok(())
    }

    #[func]
    fn populate_construction(&mut self) {
        if let Err(result) = self.try_populate_construction() {
            println!("{}", result)
        }
    }

    fn try_populate_construction(&mut self) -> Result<()> {
        let config = self.try_get_config()?;
        let mut construction_node = self.try_get_option_button(&self.construction_node)?;
        let original_construction =
            construction_node.get_item_text(construction_node.get_selected_id());

        let armor_shape_node = self.try_get_option_button(&self.armor_shape_node)?;
        let armor_shape_name = armor_shape_node
            .get_item_text(armor_shape_node.get_selected_id())
            .to_string();
        let used_locations = config
            .find_shape(armor_shape_name.as_ref())
            .map(ArmorShape::covered_locations);

        let allowed_constructions = self
            .find_material(&config)
            .map(|m| m.construction().to_vec());

        construction_node.clear();
        for (idx, construction) in config.constructions().iter().enumerate() {
            let idx: i32 = idx.try_into()?;
            construction_node
                .add_item_ex(construction.name().into())
                .id(idx)
                .done();

            // Should it be disabled?
            if let Ok(used_locations) = used_locations {
                if let Ok(ref allowed_constructions) = allowed_constructions {
                    match construction.is_eligible(used_locations, allowed_constructions.as_slice())
                    {
                        Ok(()) => {
                            construction_node.set_item_disabled(idx, false);
                            construction_node.set_item_tooltip(
                                idx,
                                serde_yaml::to_string(construction).unwrap().to_godot(),
                            );
                        }
                        Err(reason) => {
                            construction_node.set_item_disabled(idx, true);
                            construction_node.set_item_tooltip(idx, reason.into())
                        }
                    }
                } else {
                    construction_node.set_item_disabled(idx, true);
                    construction_node.set_item_tooltip(idx, "Could not find the material".into())
                }
            } else {
                construction_node.set_item_disabled(idx, true);
                construction_node.set_item_tooltip(idx, "Could not find the armor shape".into())
            }

            // Select it if not disabled and the name matches
            if original_construction == construction.name().into()
                && !construction_node.is_item_disabled(idx)
            {
                construction_node.select(idx);
            }
        }

        Ok(())
    }

    #[func]
    fn populate_concealment(&mut self) {
        if let Err(result) = self.try_populate_concealment() {
            println!("{}", result)
        }
    }

    fn try_populate_concealment(&mut self) -> Result<()> {
        let config = self.try_get_config()?;
        let mut concealment_node = self.try_get_option_button(&self.concealment_node)?;
        let original_concealment_name = concealment_node
            .get_item_text(concealment_node.get_selected_id())
            .to_string();
        let original_concealment: Result<Concealment, _> =
            serde_yaml::from_str(&original_concealment_name);
        let max_dr = self.find_material(&config).map(|m| m.max_dr());
        let dr = self.try_get_dr();

        concealment_node.clear();
        for (idx, concealment) in Concealment::all_variants().iter().enumerate() {
            let idx: i32 = idx.try_into()?;
            concealment_node
                .add_item_ex(serde_yaml::to_string(concealment)?.into())
                .id(idx)
                .done();

            if let Ok(ref original_concealment) = original_concealment {
                if let Ok(max_dr) = max_dr {
                    if let Ok(dr) = dr {
                        match concealment.is_eligible(dr, max_dr) {
                            Ok(()) => {
                                concealment_node.set_item_disabled(idx, false);
                                concealment_node.set_item_tooltip(
                                    idx,
                                    serde_yaml::to_string(concealment).unwrap().to_godot(),
                                );
                            }
                            Err(reason) => {
                                concealment_node.set_item_disabled(idx, true);
                                concealment_node.set_item_tooltip(idx, reason.into());
                            }
                        }
                    } else {
                        concealment_node.set_item_disabled(idx, true);
                        concealment_node.set_item_tooltip(idx, "Could not parse DR".into())
                    }
                } else {
                    concealment_node.set_item_disabled(idx, true);
                    concealment_node
                        .set_item_tooltip(idx, "Could not parse Material for its max DR".into())
                }

                if *original_concealment == *concealment && !concealment_node.is_item_disabled(idx)
                {
                    concealment_node.select(idx);
                }
            } else {
                concealment_node.set_item_disabled(idx, true);
                concealment_node.set_item_tooltip(
                    idx,
                    format!("Could not parse {original_concealment_name} as a type of concealment")
                        .into(),
                )
            }
        }
        Ok(())
    }

    fn try_clear_accessories_buttons(&mut self) -> Result<()> {
        let mut accessory_root: Gd<BoxContainer> = self
            .base()
            .try_get_node_as::<BoxContainer>(self.accessories_node.clone())
            .ok_or_eyre(format!(
                "No child BoxContainer node found at '{}'",
                self.accessories_node,
            ))?;
        for mut node in accessory_root.get_children().iter_shared() {
            node.queue_free();
            accessory_root.remove_child(node);
        }
        Ok(())
    }

    fn try_connect_new_accessory_button(&mut self) -> Result<()> {
        let callable = Callable::from_object_method(&self.base_mut(), "add_selected_accessory");
        self.base()
            .try_get_node_as::<MenuButton>(self.add_accessory_node.clone())
            .unwrap()
            .get_popup()
            .unwrap()
            .connect("id_pressed".into(), callable);
        Ok(())
    }

    #[func]
    fn remove_accessory_button(&mut self, button_path: NodePath) {
        let mut accessory_root: Gd<BoxContainer> = self
            .base()
            .get_node_as::<BoxContainer>(self.accessories_node.clone());
        let mut node = self.base().get_node_as::<Button>(button_path);
        node.queue_free();
        accessory_root.remove_child(node.upcast());
        self.base_mut()
            .emit_signal("accessories_changed".into(), &[]);
    }

    #[func]
    fn add_selected_accessory(&mut self, id: i32) {
        if let Err(result) = self.try_add_selected_accessory(id) {
            println!("{}", result)
        }
        self.base_mut()
            .emit_signal("accessories_changed".into(), &[]);
    }

    fn try_add_selected_accessory(&mut self, id: i32) -> Result<()> {
        let add_accessory_popup = self
            .base()
            .try_get_node_as::<MenuButton>(self.add_accessory_node.clone())
            .ok_or_eyre("Can't get the add accessory button")?
            .get_popup()
            .ok_or_eyre("Can't get the add accessory button's popup")?;
        let accessories_node = self
            .base()
            .try_get_node_as::<VBoxContainer>(self.accessories_node.clone())
            .ok_or_eyre("Can't get the accessory node")?;

        let index = add_accessory_popup.get_item_index(id);
        let accessory_text = add_accessory_popup.get_item_text(index);
        self.add_accessory_button(
            accessories_node.upcast(),
            accessory_text.to_string().as_str(),
        );
        Ok(())
    }

    fn add_accessory_button(&self, mut parent_node: Gd<Node>, button_name: &str) {
        let mut new_accessory = Button::new_alloc();
        new_accessory.set_text(button_name.to_godot());
        new_accessory.set_text_alignment(HorizontalAlignment::LEFT);
        parent_node.add_child(new_accessory.clone().upcast());

        let path = self.base().get_path_to(new_accessory.clone().upcast());
        new_accessory.connect(
            "pressed".into(),
            self.base()
                .callable("remove_accessory_button")
                .bindv(array![path.to_variant()]),
        );
    }

    fn try_populate_accessories_buttons(&mut self, accessories: &[String]) -> Result<()> {
        self.try_clear_accessories_buttons()?;
        let accessories_node = self
            .base()
            .try_get_node_as::<VBoxContainer>(self.accessories_node.clone())
            .ok_or_eyre(format!(
                "No VBoxContainer node found at '{}'",
                self.accessories_node,
            ))?;
        for accessory in accessories {
            self.add_accessory_button(accessories_node.clone().upcast(), accessory.as_str());
        }
        Ok(())
    }

    #[func]
    fn populate_accessories(&mut self) {
        if let Err(result) = self.try_populate_accessories() {
            println!("{}", result)
        }
    }

    fn try_populate_accessories(&mut self) -> Result<()> {
        let config = self.try_get_config()?;
        let add_accessory_path = self.add_accessory_node.clone();
        let add_accessory_node = self.try_get_menu_button(&add_accessory_path)?;
        let mut menu = add_accessory_node
            .get_popup()
            .ok_or_eyre("Can't get popup for add accessory button")?;
        let accessories = self.try_get_accessories()?;

        menu.clear();
        let mut reasons: Vec<String> = vec![];

        let tech_level = self.try_get_tech_level();
        if tech_level.as_ref().is_err() {
            reasons.push("Cannot parse tech level".to_string());
        }
        let material = self.find_material(&config);
        let material_name = material.as_ref().map(|m| m.name());
        let unscaled_max_dr = material.as_ref().map(|m| m.unscaled_max_dr());
        if material.is_err() {
            reasons.push("Cannot find material".to_string());
        }
        let dr = self.try_get_dr();
        if dr.as_ref().is_err() {
            reasons.push("Cannot parse DR".to_string());
        }
        let construction = self.find_construction(&config);
        let is_rigid = construction.as_ref().map(|c| {
            if let Ok(dr) = &dr {
                if let Ok(unscaled_max_dr) = unscaled_max_dr {
                    c.is_rigid(*dr, unscaled_max_dr)
                } else {
                    false
                }
            } else {
                false
            }
        });
        if construction.as_ref().is_err() {
            reasons.push("Cannot find construction".to_string());
        }
        let armor_shape = self.find_armor_shape(&config);
        let hit_locations = armor_shape.as_ref().map(|s| s.covered_locations());
        let coverage = armor_shape.as_ref().map(|s| s.percentage_covered());
        if armor_shape.as_ref().is_err() {
            reasons.push("Cannot find armor shape".to_string())
        }

        let all_disabled = !reasons.is_empty();
        for (id, accessory) in config.accessories().iter().enumerate() {
            if accessories.contains(&accessory.name().to_string()) {
                // Accessory is already in the buttons, don't add to dropdown
                continue;
            }
            let id: i32 = id.try_into()?;
            menu.add_item_ex(accessory.name().into()).id(id).done();
            let index = menu.get_item_index(id);
            if all_disabled {
                menu.set_item_disabled(index, true);
                menu.set_item_tooltip(index, reasons.join(", ").into());
                continue;
            }
            if let Ok(dr) = dr {
                match accessory.is_eligible(
                    tech_level.as_ref().unwrap(),
                    material_name.unwrap(),
                    is_rigid.unwrap(),
                    hit_locations.unwrap(),
                    dr,
                    coverage.unwrap(),
                    accessories.as_ref(),
                ) {
                    Ok(_) => {
                        menu.set_item_disabled(index, false);
                        menu.set_item_tooltip(
                            index,
                            serde_yaml::to_string(accessory).unwrap().to_godot(),
                        );
                    }
                    Err(reason) => {
                        menu.set_item_disabled(index, true);
                        menu.set_item_tooltip(index, reason.join(", ").into());
                    }
                }
            }
        }

        Ok(())
    }

    fn try_get_config(&mut self) -> Result<Config> {
        let config_path = self.config_node_path.clone();
        let config_text = self.get_textedit_text(&config_path)?;
        let config = serde_yaml::from_str(config_text.as_ref())?;
        Ok(config)
    }

    fn try_set_config(&mut self, config: &Config) -> Result<()> {
        let config_text = serde_yaml::to_string(config)?;
        let config_path = self.config_node_path.clone();
        self.base_mut()
            .try_get_node_as::<TextEdit>(config_path.clone())
            .ok_or_eyre(format!("No TextEdit config node at {}", config_path))?
            .set_text(config_text.into());
        Ok(())
    }

    #[func]
    fn populate_dropdowns(&mut self) {
        if let Err(result) = self.try_populate_dropdowns() {
            println!("{}", result)
        }
    }

    fn try_populate_dropdowns(&mut self) -> Result<()> {
        self.try_populate_armor_shape()?;
        self.try_populate_material()?;
        self.try_populate_construction()?;
        self.try_populate_concealment()?;
        self.try_populate_accessories()?;

        Ok(())
    }

    #[func]
    fn save_and_close(&mut self) {
        if let Err(result) = self
            .try_save_to_config()
            .and_then(|()| self.try_remove_from_parent())
        {
            println!("{}", result)
        }
    }

    fn try_save_to_config(&mut self) -> Result<()> {
        let mut config = self.try_get_config()?;
        let new_design = self.try_to_design()?;
        config.designs_mut().push(new_design);
        self.try_set_config(&config)?;

        Ok(())
    }

    #[func]
    fn close(&mut self) {
        if let Err(result) = self.try_remove_from_parent() {
            println!("{}", result)
        }
    }

    fn try_remove_from_parent(&mut self) -> Result<()> {
        let mut own_node = self.base_mut().clone();
        let mut parent_node = own_node
            .get_parent()
            .ok_or_eyre("Could not get parent node of armor design view")?;

        own_node.queue_free();
        parent_node.remove_child(own_node.upcast());

        Ok(())
    }

    pub(crate) fn new_design(config_node_path: NodePath) -> Gd<Self> {
        let packed: Gd<PackedScene> = load(SCENE_PATH);
        let mut instantiated = packed.instantiate_as::<Self>();
        {
            let mut bound = instantiated.bind_mut();
            bound.sync_name();
            bound.config_node_path = config_node_path;
            if let Err(result) = bound.try_connect_new_accessory_button() {
                println!("{}", result)
            }
        }
        instantiated
    }

    /// scene from Design
    pub(crate) fn try_from_design(design: Design, config_node_path: NodePath) -> Result<Gd<Self>> {
        let scene: Gd<PackedScene> = load(SCENE_PATH);
        let mut root = scene.instantiate_as::<Self>();
        root.bind_mut().config_node_path = config_node_path;

        root.bind_mut().try_connect_new_accessory_button()?;

        Self::set_textedit_text(
            &root,
            &root.bind().tech_level_node,
            design.tech_level().to_string(),
        )?;
        Self::set_textedit_text(&root, &root.bind().name_node, design.name().to_string())?;

        // The only information we have is the selected armor shape, add one item which is the selected value
        let shape_path = root.bind().armor_shape_node.clone();
        let mut armor_shape_node: Gd<OptionButton> =
            root.bind_mut().try_get_option_button(&shape_path)?;
        armor_shape_node.clear();
        armor_shape_node
            .add_item_ex(design.shape().into())
            .id(0)
            .done();
        armor_shape_node.select(0);

        Self::set_textedit_text(
            &root,
            &root.bind().size_modifier_node,
            design.size_modifier().to_string(),
        )?;

        let material_path = root.bind().material_node.clone();
        let mut material_node = root.bind_mut().try_get_option_button(&material_path)?;
        material_node.clear();
        material_node
            .add_item_ex(design.material().into())
            .id(0)
            .done();
        material_node.select(0);

        let construction_path = root.bind().construction_node.clone();
        let mut construction_node = root.bind_mut().try_get_option_button(&construction_path)?;
        construction_node.clear();
        construction_node
            .add_item_ex(design.construction().into())
            .id(0)
            .done();
        construction_node.select(0);

        Self::set_textedit_text(&root, &root.bind().dr_node, design.dr().to_string())?;
        Self::set_textedit_text(
            &root,
            &root.bind().legality_class_node,
            design.legality_class().to_string(),
        )?;

        let concealment_path = root.bind().concealment_node.clone();
        let mut concealment_node = root.bind_mut().try_get_option_button(&concealment_path)?;
        concealment_node.clear();
        concealment_node
            .add_item_ex(serde_yaml::to_string(design.concealment())?.into())
            .id(0)
            .done();
        concealment_node.select(0);

        root.set_name(format!("{}{}", ARMOR_ICON, design.name()).into());

        root.bind_mut()
            .try_populate_accessories_buttons(design.accessories())?;

        // Populate Exoskeletons here!
        let exoskeleton_type_path = root.bind().get_exoskeleton_type_node();
        let mut exoskeleton_type_node = root
            .bind_mut()
            .try_get_option_button(&exoskeleton_type_path)?;
        let exoskeleton_type = design.exoskeleton.type_name();
        let item_count = exoskeleton_type_node.get_item_count();
        for n in 0..item_count {
            if exoskeleton_type_node.get_item_text(n).to_string().as_str() == exoskeleton_type {
                exoskeleton_type_node.select(n)
            }
        }

        let exoskeleton_strength_path = root.bind().get_exoskeleton_strength_bonus_node();
        Self::set_textedit_text(
            &root,
            &exoskeleton_strength_path,
            design.exoskeleton.lifting_strength_bonus().to_string(),
        )?;

        let exoskeleton_civilian_path = root.bind().get_exoskeleton_civilian_node();
        let civilian = design.exoskeleton.is_civilian();
        root.try_get_node_as::<CheckBox>(exoskeleton_civilian_path.clone())
            .ok_or_eyre(format!(
                "No CheckBox found at '{}'",
                exoskeleton_civilian_path
            ))?
            .set_pressed(civilian);

        Ok(root)
    }

    fn try_get_accessories(&self) -> Result<Vec<String>> {
        let accessories_node = self
            .base()
            .try_get_node_as::<VBoxContainer>(self.accessories_node.clone())
            .ok_or_eyre(format!(
                "No VBoxContainer node found at '{}'",
                self.accessories_node,
            ))?;
        let mut accessories = vec![];
        for child in accessories_node.get_children().iter_shared() {
            let child_path = child.get_path();
            let button = child
                .try_cast::<Button>()
                .map_err(|e| eyre!("Could not cast node {} to Button: {}", child_path, e))?;
            accessories.push(button.get_text().to_string());
        }
        Ok(accessories)
    }

    fn try_get_exoskeleton(&self) -> Result<Exoskeleton> {
        let exoskeleton_type = self
            .base()
            .try_get_node_as::<OptionButton>(self.exoskeleton_type_node.clone())
            .ok_or_eyre(format!(
                "No OptionButton node found at '{}'",
                self.exoskeleton_type_node
            ))?
            .get_text()
            .to_string();
        let exoskeleton_strength = self.get_textedit_text(&self.exoskeleton_strength_bonus_node)?;
        let civilian = self
            .base()
            .try_get_node_as::<CheckBox>(self.exoskeleton_civilian_node.clone())
            .ok_or_eyre(format!(
                "No CheckBox node found at '{}'",
                self.exoskeleton_civilian_node
            ))?
            .is_pressed();
        match exoskeleton_type.as_str() {
            "None" => Ok(Exoskeleton::None),
            "ExoskeletonFrame" => Ok(Exoskeleton::ExoskeletonFrame {
                strength_bonus: exoskeleton_strength.parse()?,
                civilian,
            }),
            "CombatWalker" => Ok(Exoskeleton::CombatWalker {
                strength_bonus: exoskeleton_strength.parse()?,
                civilian,
            }),
            "CyberSuit" => Ok(Exoskeleton::CyberSuit {
                strength_bonus: exoskeleton_strength.parse()?,
                civilian,
            }),
            _ => Err(eyre!(
                "Unexpected exoskeleton type  {}",
                exoskeleton_type.as_str()
            )),
        }
    }

    pub(crate) fn try_to_design(&self) -> Result<Design> {
        let concealment =
            serde_yaml::from_str(self.get_button_text(&self.concealment_node)?.as_ref())?;
        Ok(Design {
            tech_level: self.try_get_tech_level()?,
            name: self.get_textedit_text(&self.name_node)?,
            shape: self.get_button_text(&self.armor_shape_node)?,
            size_modifier: self.get_textedit_text(&self.size_modifier_node)?.parse()?,
            material: self.get_button_text(&self.material_node)?,
            construction: self.get_button_text(&self.construction_node)?,
            dr: self.get_textedit_text(&self.dr_node)?.parse()?,
            accessories: self.try_get_accessories()?,
            legality_class: self.get_textedit_text(&self.legality_class_node)?.parse()?,
            concealment,
            exoskeleton: self.try_get_exoskeleton()?,
        })
    }
}
