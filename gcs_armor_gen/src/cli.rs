use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub(crate) struct Args {
    /// Output file, to stdout if not set
    #[arg(short, long)]
    pub(crate) output: Option<String>,

    /// Config files, Searches for config.yaml and config/*.yaml if not set
    #[arg(short, long)]
    pub(crate) config: Option<Vec<String>>,

    /// Armor Design(s), reads from stdin if not set and config has no designs
    #[arg(short, long)]
    pub(crate) design: Option<Vec<String>>,

    /// Prints the config that was loaded to stderr
    #[arg(short, long, action = clap::ArgAction::SetTrue)]
    pub(crate) print_config: bool,
}
