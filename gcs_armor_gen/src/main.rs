use lib_gcs_armor_gen::*;

mod cli;

use armor_design::ArmorDesign;
use clap::Parser;
use color_eyre::eyre::{bail, Result};
use gcs::GcsEquipmentLib;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::path::Path;
use std::path::PathBuf;

use gcs::GcsEquipmentItem;

fn load_config(path: impl AsRef<Path>) -> Result<config::Config> {
    let config: config::Config = serde_yaml::from_reader(BufReader::new(File::open(path)?))?;
    Ok(config)
}

fn yaml_files_in_dir(dir: impl AsRef<Path>) -> Result<Vec<PathBuf>> {
    let mut files: Vec<PathBuf> = vec![];
    for ent in dir.as_ref().read_dir()?.flatten() {
        if let Ok(ft) = ent.file_type() {
            if ft.is_file()
                && (ent.path().to_string_lossy().ends_with(".yml")
                    || ent.path().to_string_lossy().ends_with(".yaml"))
            {
                files.push(ent.path())
            }
        }
    }
    if files.is_empty() {
        bail!(
            "No config files found in {}",
            dir.as_ref().to_string_lossy()
        );
    }
    files.sort();
    Ok(files)
}

fn process_config(cli: &cli::Args) -> Result<config::Config> {
    let mut config: config::Config = Default::default();
    match &cli.config {
        // if cli specifies config paths, load them
        Some(config_paths) => {
            for path in config_paths {
                let path = std::path::Path::new(&path);
                match path.is_dir() {
                    true => {
                        for file in yaml_files_in_dir(path)? {
                            config.append(load_config(&file)?);
                        }
                    }
                    false => {
                        config.append(load_config(path)?);
                    }
                }
            }
        }
        // if no config paths specified, try to load config.yaml or every yaml file in ./config/
        None => {
            if Path::new("config.yaml").is_file() {
                config.append(load_config("config.yaml")?)
            } else if Path::new("config").is_dir() {
                for file in yaml_files_in_dir("config")? {
                    config.append(load_config(&file)?);
                }
            } else {
                bail!("No config was specified and no 'config.yaml' or 'config' directory found");
            }
        }
    }
    config.validate()?;
    Ok(config)
}

fn process_designs(cli: &cli::Args, config: &config::Config) -> Result<config::Config> {
    match &cli.design {
        // if specified, load each path as a vector of SerialArmorDesigns
        Some(design_paths) => {
            let mut designs: Vec<config::design::Design> = vec![];
            for f in design_paths {
                let mut new_designs: Vec<config::design::Design> =
                    serde_yaml::from_reader(BufReader::new(File::open(f)?))?;
                designs.append(&mut new_designs);
            }
            Ok(designs.into())
        }
        // if not specified, and config has no designs, load designs from stdin as vector of
        // SerialArmorDesigns
        None => match config.designs().is_empty() {
            true => {
                let designs: Vec<config::design::Design> =
                    serde_yaml::from_reader(std::io::stdin())?;
                Ok(designs.into())
            }
            false => Ok(Default::default()),
        },
    }
}

fn process_args() -> Result<()> {
    let cli = cli::Args::parse();

    let mut config = process_config(&cli)?;
    if cli.print_config {
        let stderr = std::io::stderr();
        serde_yaml::to_writer(stderr, &config)?;
    }
    let designs = process_designs(&cli, &config)?;
    config.append(designs);
    config.validate()?;
    let loaded_designs: Vec<ArmorDesign> = armor_design::load_armor_designs(&config)?;

    let items = loaded_designs
        .into_iter()
        .map(|s| s.into())
        .collect::<Vec<GcsEquipmentItem>>();
    let lib: GcsEquipmentLib = items.into();

    match &cli.output {
        Some(outfile) => {
            serde_json::to_writer_pretty(BufWriter::new(File::create(outfile)?), &lib)?;
        }
        None => {
            serde_json::to_writer_pretty(std::io::stdout(), &lib)?;
        }
    }

    Ok(())
}

fn main() -> Result<()> {
    process_args()?;
    Ok(())
}
